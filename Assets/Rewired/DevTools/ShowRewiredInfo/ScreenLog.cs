﻿// Copyright (c) 2014 Augie R. Maddox, Guavaman Enterprises. All rights reserved.

using System.Collections.Generic;
using UnityEngine;


public static class ScreenLog {

    private static int maxLines = 12;

    public static string debugInfo;
    private static List<string> log;
    private static bool pause;

    public static void SetMaxLines(int lines) {
        maxLines = lines;
    }

    public static void Log(string s) {
        //Debug.Log(s);

        if(log == null) log = new List<string>();
        log.Add(s);

        if(log.Count > maxLines) log.RemoveAt(0);
    }

    public static void Log(object s) {
        Log(s.ToString());
    }

    public static void Update(bool logToScreen) {
        if(logToScreen) return;
        debugInfo = "";
    }

    public static void Draw(bool logToScreen, GUIText guiText) {
        if(guiText == null) return;

        if(logToScreen) { // log to GUIText for testing in builds
            if(log == null) return;
            string str = "";
            for(int i = 0; i < log.Count; i++) {
                str += log[i] + "\n";
            }
            guiText.text = str;
        } else {
            guiText.text = debugInfo;
        }
    }
}
