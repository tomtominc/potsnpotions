﻿// Copyright (c) 2014 Augie R. Maddox, Guavaman Enterprises. All rights reserved.

using UnityEngine;
using System.Collections.Generic;
using Rewired;

[RequireComponent(typeof(GUIText))]
public class Logger : MonoBehaviour {

    public bool showAxes = true;
    public bool showButtonDown = false;
    public bool showButtonUp = false;
    public bool showButtonHeld = false;
    public bool showButtonDoublePressHold = false;
    public bool showButtonDoublePressDown = false;
    public bool logToScreen;
    public int screenLogMaxLines = 12;
    public bool dontDestroyOnLoad = true;
    
    private GUIText text;


    private void Awake() {
        // Prevent this game object from being destroyed if level is changed
        if(dontDestroyOnLoad) Object.DontDestroyOnLoad(this);

        // Subscribe to device change events
        ReInput.ControllerConnectedEvent += JoystickConnected;
        ReInput.ControllerDisconnectedEvent += JoystickDisconnected;
        ReInput.ControllerPreDisconnectEvent += JoystickPreDisconnected;

        // GUI logging
        text = GetComponent<GUIText>();

        ScreenLog.SetMaxLines(screenLogMaxLines);
    }

    private float unityMoveX;
    private float unityMoveY;
    private float rewiredMoveX;
    private float rewiredMoveY;

    private void Update() {
        if(!ReInput.isReady) return;
        
        ScreenLog.Update(logToScreen); // clear log if necessary

        // Write data to screen
        ScreenLog.Draw(logToScreen, text);

        IList<Player> players = ReInput.players.AllPlayers;

        for(int i = 0; i < players.Count; i++) {
            Player player = players[i];

            IList<InputAction> actions = ReInput.mapping.Actions;
            for(int j = 0; j < actions.Count; j++) {
                InputAction action = actions[j];

//                bool buttonDown;
//                bool buttonUp;
//                bool buttonHeld;
                float axisValue;
                
                //buttonValue = p.GetNegativeButtonDown(action.id);
                //buttonValue = p.GetButtonDoublePressDown(action.id);
                //buttonValue = p.GetButtonDoublePressHold(action.id);
                axisValue = player.GetAxis(action.id);

                if(showButtonDown && player.GetButtonDown(action.id)) ScreenLog.Log(player.descriptiveName + ": " + action.descriptiveName + " DOWN");
                if(showButtonHeld && player.GetButton(action.id)) ScreenLog.Log(player.descriptiveName + ": " + action.descriptiveName + " HELD " + player.GetButtonTimePressed(action.id));
                if(showButtonUp && player.GetButtonUp(action.id)) ScreenLog.Log(player.descriptiveName + ": " + action.descriptiveName + " UP");
                if(showButtonDoublePressHold && player.GetButtonDoublePressHold(action.id)) ScreenLog.Log(player.descriptiveName + ": " + action.descriptiveName + " DOUBLE PRESS HOLD");
                if(showButtonDoublePressDown && player.GetButtonDoublePressDown(action.id)) ScreenLog.Log(player.descriptiveName + ": " + action.descriptiveName + " DOUBLE PRESS DOWN");
                if(showAxes && axisValue != 0.0f) ScreenLog.Log(player.GetAxis(action.id) + " " + player.descriptiveName + ": " + action.descriptiveName + " " + axisValue);
            }
        }
    }

    private void JoystickConnected(ControllerStatusChangedEventArgs args) {
        Joystick j = ReInput.controllers.GetController<Joystick>(args.controllerId);
        string msg = "Controller connected! Name: " + args.name + "\n";
        msg += "name = " + args.controllerType + "\n";
        msg += "rewiredId = " + args.controllerId + "\n";
        msg += "systemId = " + j.systemId + "\n";
        msg += "unityId = " + j.unityId + "\n";
        if(logToScreen) ScreenLog.Log(msg);
        else Debug.LogWarning(msg);
    }

    private void JoystickPreDisconnected(ControllerStatusChangedEventArgs args) {
        Joystick j = ReInput.controllers.GetController<Joystick>(args.controllerId);
        string msg = "Controller pre-disconnected! Name: " + args.name + "\n";
        msg += "name = " + args.controllerType + "\n";
        msg += "rewiredId = " + args.controllerId + "\n";
        msg += "systemId = " + j.systemId + "\n";
        msg += "unityId = " + j.unityId + "\n";
        if(logToScreen) ScreenLog.Log(msg);
        else Debug.LogWarning(msg);
    }

    private void JoystickDisconnected(ControllerStatusChangedEventArgs args) {
        string msg = "Controller disconnected! Name: " + args.name + " Joystick Id: " + args.controllerId + " Type: " + args.controllerType;
        if(logToScreen) ScreenLog.Log(msg);
        else Debug.LogWarning(msg);
    }

}
