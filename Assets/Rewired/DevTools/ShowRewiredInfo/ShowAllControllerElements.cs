﻿// Copyright (c) 2014 Augie R. Maddox, Guavaman Enterprises. All rights reserved.

using UnityEngine;
using Rewired;

public class ShowAllControllerElements : MonoBehaviour {

    public bool showAxes = true;
    public bool showAxes2D = false;
    public bool showButtons = true;

    public float columnWidth = 175.0f;
    public Vector2 offset;
    public Vector2 size = Vector2.one;

    Vector2 scrollPos;

	void OnGUI () {
        if(!ReInput.isReady) return;

        GUILayout.BeginArea(new Rect(Screen.width * offset.x, Screen.height * offset.y, Screen.width * size.x, Screen.height * size.y));
        scrollPos = GUILayout.BeginScrollView(scrollPos);

        GUILayout.Label("All Rewired Joystick element values:");

        if(ReInput.controllers.joystickCount == 0) {
            GUILayout.Label("No joysticks found!");
        
        } else {

            GUILayout.BeginHorizontal();
			int index = 0;
            foreach(Joystick j in ReInput.controllers.Joysticks) {

                GUILayout.BeginVertical(GUILayout.Width(columnWidth), GUILayout.ExpandWidth(false));

				string text = "[" + index  +"]";
				index++;
                text += "\n" + j.hardwareName + "\n";

                if(showAxes) {
                    for(int i = 0; i < j.axisCount; i++) {
                        text += j.Axes[i].name + " = " + j.Axes[i].value.ToString("F3") + " [" + j.Axes[i].valueRaw.ToString("F3") + "]\n";
                    }
                }

                if(showAxes2D) {
                    for(int i = 0; i < j.axis2DCount; i++) {
                        text += j.Axes2D[i].name + " = " + Rewired.Utils.StringTools.ToString(j.Axes2D[i].value) + " [" + Rewired.Utils.StringTools.ToString(j.Axes2D[i].valueRaw) + "]\n";
                    }
                }

                if(showButtons) {
                    for(int i = 0; i < j.buttonCount; i++) {
                        text += j.Buttons[i].name + " = " + j.Buttons[i].value + "\n";
                    }
                }

                GUILayout.Label(text);

                GUILayout.EndVertical();
            }


            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
	}


}
