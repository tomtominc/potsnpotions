// Copyright (c) 2014 Augie R. Maddox, Guavaman Enterprises. All rights reserved.

using UnityEngine;
using Rewired;

public class ListControllers : MonoBehaviour {

    public bool listByPlayer = true;
    public bool listAll = true;
    public Vector2 offset;
    public Vector2 size = Vector2.one;

    Vector2 scrollPos;

    void OnGUI() {
        if(!ReInput.isReady) return;

        GUILayout.BeginArea(new Rect(Screen.width * offset.x, Screen.height * offset.y, Screen.width * size.x, Screen.height * size.y));
        scrollPos = GUILayout.BeginScrollView(scrollPos);

        string s = "";

        if(listByPlayer) {
            GUILayout.Label("Assigned Joysticks by Player:");

            foreach(Player p in ReInput.players.AllPlayers) {
                s += p.name + ": " + p.controllers.joystickCount + " joystick" + (p.controllers.joystickCount != 1 ? "s" : "") + "\n";
                foreach(Joystick j in p.controllers.Joysticks) {
                    s += GetJoystickData(j);
                }
                s += "\n";
            }
        }

        if(listAll) {
            s += "All Joysticks:\n\n";
            foreach(Joystick j in ReInput.controllers.Joysticks) {
                s += GetJoystickData(j);
                s += "\n";
            }
        }


        GUILayout.Label(s);

        GUILayout.EndScrollView();
        GUILayout.EndArea();
	}

    private string GetJoystickData(Joystick j) {
        string s = "";
        s += "Joystick name = " + j.name + "\n";
        s += "Hardware name = " + j.hardwareName + "\n";
        s += "Rewired id = " + j.id + "\n";
        s += "systemId = " + j.systemId + "\n";
        s += "GUID = " + j.hardwareTypeGuid + "\n";
        s += "Button count = " + j.buttonCount + "\n";
        s += "Axis count = " + j.axisCount + "\n";
        return s;
    }
}
