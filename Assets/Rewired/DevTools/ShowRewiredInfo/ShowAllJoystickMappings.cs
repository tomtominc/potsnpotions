﻿using UnityEngine;
using System.Collections.Generic;
using Rewired;

public class ShowAllJoystickMappings : MonoBehaviour {

    public bool showAxes = true;
    public bool showButtons = true;

    public float columnWidth = 175.0f;
    public Vector2 offset;
    public Vector2 size = Vector2.one;

    Vector2 scrollPos;

	void OnGUI () {
        

        GUILayout.BeginArea(new Rect(Screen.width * offset.x, Screen.height * offset.y, Screen.width * size.x, Screen.height * size.y));
        scrollPos = GUILayout.BeginScrollView(scrollPos);

        GUILayout.Label("All Rewired Joystick mappings:");

        if(ReInput.controllers.joystickCount == 0) {
            GUILayout.Label("No joysticks found!");
        
        } else {

            GUILayout.BeginHorizontal();

            foreach(Player p in ReInput.players.Players) {

                string text = "";

                GUILayout.BeginVertical(GUILayout.Width(columnWidth), GUILayout.ExpandWidth(false));

                text += "Player [" + p.id + "]\n\n";

                foreach(Joystick j in p.controllers.Joysticks) {

                    text += j.hardwareName + "\n";

                    IList<JoystickMap> maps = p.controllers.maps.GetMaps<JoystickMap>(j.id);
                    text += "Contains " + maps.Count + " JoystickMaps\n\n";

                    for(int i = 0; i < maps.Count; i++) {
                        JoystickMap map = maps[i];
                        
                        text += "Joystick Map [" + i + "]\n";
                        text += "Is Enabled   = " + map.enabled + "\n";
                        text += "Axis Maps    = " + map.axisMapCount + "\n";
                        text += "Button Maps  = " + map.buttonMapCount + "\n";
                        text += "\n";

                        if(showAxes) {
                            int count = 0;
                            foreach(var aem in map.AxisMaps) {
                                InputAction action = ReInput.mapping.GetAction(aem.actionId);
                                text += "Axis Map [" + count + "]\n";
                                text += "Axis Type  = " + aem.axisType + "\n";
                                text += "Axis Range = " + aem.axisRange + "\n";
                                text += "Axis Index = " + aem.elementIndex + "\n";
                                text += "Name       = " + aem.elementIdentifierName + "\n";
                                text += "Inverted   = " + aem.invert + "\n";
                                text += "Action     = " + (action == null ? "NULL" : action.name) + "\n";
                                text += "\n";
                                count++;
                            }
                        }

                        if(showButtons) {
                            int count = 0;
                            foreach(var aem in map.ButtonMaps) {
                                InputAction action = ReInput.mapping.GetAction(aem.actionId);
                                text += "Button Map [" + count + "]\n";
                                text += "Contribution = " + aem.axisContribution + "\n";
                                text += "Button Index = " + aem.elementIndex + "\n";
                                text += "Name         = " + aem.elementIdentifierName + "\n";
                                text += "Action       = " + (action == null ? "NULL" : action.name) + "\n";
                                text += "\n";
                                count++;
                            }
                        }
                        text += "\n";
                    }

                    GUILayout.Label(text);
                }

                GUILayout.EndVertical();
            }


            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();
        GUILayout.EndArea();
	}


}
