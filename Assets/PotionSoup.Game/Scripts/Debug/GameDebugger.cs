using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;
using PotionSoup.SceneManagement;

namespace PotionSoup.GameLogic
{
    public class GameDebugger : MonoBehaviour
    {
        public CharacterData player_p1;
        public CharacterData player_p2;
        public CharacterData player_p3;
        public CharacterData player_p4;

		public RoomDataName debugRoom = RoomDataName.Default;

		public int targetCoinCount = 5;

        // Use this for initialization
        void Awake()
		{
			if (GameEnviornment.HasSceneBeenLoaded(StringManager.scene_main_menu))
			{
				gameObject.SetActive(false); return;
			}

			Init();
        }


		void Init ()
		{
			string key_character_select = StringManager.key_character_save;

			DataBridge.SetValue<CharacterData>(PlayerID.one.ToString() + key_character_select, player_p1);
			DataBridge.SetValue<CharacterData>(PlayerID.two.ToString() + key_character_select,player_p2 );
			DataBridge.SetValue<CharacterData>(PlayerID.three.ToString() + key_character_select,player_p3 );
			DataBridge.SetValue<CharacterData>(PlayerID.four.ToString() + key_character_select, player_p4 );
			DataBridge.SetValue<RoomDataName> ( StringManager.key_room_data, debugRoom);
			DataBridge.SetValue< int > ( StringManager.key_target_coin_count, targetCoinCount );
		}

    }
}
