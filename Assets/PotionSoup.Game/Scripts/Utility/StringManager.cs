using UnityEngine;
using System.Collections;

public class StringManager 
{
    public static string scene_main_menu = "Menu";
	public static string scene_brawl_room = "Game";

	public static string key_character_save = "_selected";
	public static string key_gametype = "game_type";
	public static string key_gametheme = "game_theme";
	public static string key_room_data = "key_room_data";
	public static string key_target_coin_count = "key_target_coin_count";

	public static string prefab_coin = "Coin";
	public static string prefab_character = "Character";

	public static string input_menu_horizontal = "horizontal_select";
	public static string input_menu_vertical = "vertical_select";
	public static string input_menu_positive = "positive";
	public static string input_menu_positive_alt1 = "positiveAlt1";
	public static string input_menu_positive_alt2 = "positiveAlt2";
	public static string input_menu_negative = "negative";
	public static string input_menu_right_alternate = "right_alternate";
	public static string input_menu_left_alternate = "left_alternate";


}
