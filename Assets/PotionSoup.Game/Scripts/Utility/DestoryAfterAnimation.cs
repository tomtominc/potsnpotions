﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.GameLogic
{
	public class DestoryAfterAnimation : MonoBehaviour 
	{
		private Animator _animator;
		private void Start ()
		{
			_animator = GetComponent<Animator>();
			Destroy(gameObject,_animator.GetCurrentAnimatorStateInfo(0).length);

		}
	}
}