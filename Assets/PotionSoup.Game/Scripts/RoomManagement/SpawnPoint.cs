﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpawnPoint : MonoBehaviour
    {
        public bool debug = true;
        public PlayerID playerId;
		public Vector2 position { get { return (Vector2)transform.position; } } 

        private SpriteRenderer spriteRenderer { get { return GetComponent<SpriteRenderer>();  } }
        private void Awake ()
        {
            if (debug == false) spriteRenderer.enabled = false;
        }

        public Vector2 Ground ()
        {
            RaycastHit2D hitPoint = Physics2D.Raycast(transform.position, Vector2.down);
            return hitPoint.point;
        }
    }
}
