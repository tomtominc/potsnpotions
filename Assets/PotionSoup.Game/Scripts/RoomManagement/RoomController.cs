﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Rotorz.Tile;
using PotionSoup.Engine2D;

namespace PotionSoup.GameLogic
{
	public class RoomController : MonoBehaviour 
	{
        public Vector3 offset;

		private Dictionary<PlayerID, SpawnPoint> _spawnPoints 
		= new Dictionary<PlayerID, SpawnPoint>();

		private TileSystem _tileSystem;

		public void Initialize()
        {
            _tileSystem = GetComponent<TileSystem>();
            GrabSpawnPoints();
        }

        private void GrabSpawnPoints()
        {
			SpawnPoint[] spawnPoints = transform.GetComponentsInChildren<SpawnPoint>();

			foreach ( var point in spawnPoints )
			{
				_spawnPoints.Add(point.playerId,point);
			}
        }

        public SpawnPoint GetSpawnPoint(PlayerID playerId)
        {
			if (_spawnPoints.ContainsKey(playerId))
			{
				return _spawnPoints[playerId];
			}

			return null;
        }

		public Vector2 GetSpawnPosition(PlayerID playerId)
		{
			return GetSpawnPoint(playerId).position;
		}

        public Vector2 roomSize
        {
            get
            {
                if (_tileSystem == null)
                {
                    _tileSystem = GetComponent<TileSystem>();
                }
                int width = _tileSystem.ColumnCount * (int)_tileSystem.CellSize.x;
                int height = _tileSystem.RowCount * (int)_tileSystem.CellSize.y;

                return new Vector2(width, height);
            }
        }


	
	}
}
