﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.GameLogic
{
	public enum RoomDataName 
	{
		Default
	}
	[System.Serializable]
	public class RoomData 
	{
		public RoomDataName name = RoomDataName.Default;
		public GameObject roomPrefab = null;
		public Sprite displayImage;
	}
}