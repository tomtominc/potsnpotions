﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PotionSoup.GameLogic
{
    [System.Serializable]
	public class RoomDatabase : ScriptableObject 
	{
        [SerializeField]
		public List<RoomData> data = new List<RoomData>();

		public RoomData GetCurrentRoom ()
		{
			RoomDataName room_name = DataBridge.GetValue<RoomDataName>
				(StringManager.key_room_data, RoomDataName.Default);
			
			return data.Where(x => x.name == room_name).First();
        }

		public RoomData GetRoomData (RoomDataName room_name)
		{
			return data.Where(x => x.name == room_name).First();
		}

		public List<RoomData> GetData()
		{
			return data;
		}
	}
}

