using UnityEditor;
using UnityEngine;
using System.Collections;
using EasyEditor;

namespace PotionSoup.GameLogic
{
	[Groups("")]
	[CustomEditor(typeof(GameManager))]
	public class GameManagerEditor : EasyEditorBase
	{
	
	}
}