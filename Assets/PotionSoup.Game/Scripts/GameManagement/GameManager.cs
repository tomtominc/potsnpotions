﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Engine2D;
using PotionSoup.Dispatcher;
using PotionSoup.MenuManagement.GameLogic;

namespace PotionSoup.GameLogic
{
	public class GameManager : Singleton < GameManager > 
	{
		public GameInfo CurrentGameInfo;

        private void Start ()
        {
            EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnSelect, null, OnCharacterSelect );
            EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnDeselect, null, OnCharacterDeselect );
        }

        public void OnCharacterSelect ( IMessage message )
        {
            Hashtable data = message.Data as Hashtable;

            PlayerID playerid = (PlayerID)data["PlayerId"];
            InterfaceObject_Character character = data ["Character"] as InterfaceObject_Character;

            Debug.Log ( "Select Player ID: " + playerid);

            CharacterData characterData = new CharacterData ( playerid, character.characterType );

            GameManager.SetPlayableCharacter (playerid, characterData);
        }

        public void OnCharacterDeselect ( IMessage message )
        {
            Hashtable data = message.Data as Hashtable;

            PlayerID playerid = (PlayerID)data["PlayerId"];

            Debug.Log ( "Deselect Player ID: " + playerid);

            GameManager.SetPlayableCharacter (playerid, null);
        }

		public static void SetPlayableCharacter (PlayerID playerId, CharacterData data )
		{
			if ( data != null)
			{
				data.value = Instance.CurrentGameInfo.initialCoinValue;
				data.playable = true;
			}

			if (Instance.CurrentGameInfo.CharacterData.ContainsKey (playerId))
			{
				Instance.CurrentGameInfo.CharacterData[playerId] = data;
			}
			else 
			{
				Instance.CurrentGameInfo.CharacterData.Add (playerId, data);
			}
		}
	}
}
