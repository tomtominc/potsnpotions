﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Engine2D;

namespace PotionSoup.GameLogic
{
	[System.Serializable]
	public class GameInfo 
	{
		public enum GameMode 
		{
			Invalid, Local, Online, Single
		}

		public GameMode gameMode { get; set; }
		public int initialCoinValue = 0;
		public List < CharacterData > playableCharacters = new List<CharacterData> ();

		public Dictionary < PlayerID , CharacterData > CharacterData
		= new Dictionary <PlayerID, CharacterData> ();
	}
}
