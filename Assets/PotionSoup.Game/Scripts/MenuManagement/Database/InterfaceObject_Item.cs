﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace PotionSoup.MenuManagement.GameLogic
{
	public class InterfaceObject_Item : InterfaceObject 
	{
		[Tooltip("The name to display for players to see.")]
        public string displayType = "Potion";

		[Tooltip("How much does this item weigh? Used in game for certain mechanics.")]
		public float weight = 16f;

		[Tooltip("How much in game currency does this cost?")]
		public int inGameCurrencyValue = 100;

		[Tooltip("How much real world money (USD) does this cost?")]
        public AssetKey priceTier;

		[Tooltip("Was this item purchased? value is true if bought with in game currency or real money.")]
		public bool purchased = false;

		[Tooltip("The label used when the item is Enabled.")]
        public AssetKey enabledLabelKey;

		[Tooltip("The label used when the item is Disabled.")]
        public AssetKey disabledLabelKey;

		[Tooltip("The label used when the item hasn't been purchased yet.")]
        public AssetKey purchaseIconKey;

		[Tooltip("The label used for when the item is enabled.")]
        public AssetKey enabledIconKey;

		[Tooltip ("The label used for when the item is disabled.")]
        public AssetKey disabledIconKey;

		[Tooltip ("The on label used for button information.")]
		public AssetKey onLabelKey;

		[Tooltip ("The off label used for button information.")]
		public AssetKey offLabelKey;

		[Tooltip ("The buy label used for button information.")]
		public AssetKey buyLabelKey;

        [Tooltip ("The animator controller that will be controlling this object" )]
        public RuntimeAnimatorController animationController;

        public UnityAction action
        {
            get {
                return () => DoAction();
            }
        }

		public Sprite statusLabel 
		{
			get {
				if (purchased == false) return (Sprite) MenuManager.GetAsset (priceTier);
                return (Sprite) MenuManager.GetAsset ( enabled ? enabledLabelKey : disabledLabelKey);
			}
		}
		public Sprite statusIcon 
		{
			get {

				if (purchased == false) return (Sprite) MenuManager.GetAsset (purchaseIconKey);
				return (Sprite) MenuManager.GetAsset (enabled ? enabledIconKey : disabledIconKey);
			}
		}

		public Sprite buttonInfoLabel
		{
			get {

				if (purchased == false) return (Sprite)MenuManager.GetAsset (buyLabelKey);
				return (Sprite)MenuManager.GetAsset (enabled ? offLabelKey : onLabelKey);
			}
		}

		// used to turn things off, so component.enabled = !purchased; 
		public bool ReversedPurchased
		{
			get {
				return !purchased;
			}
		}

        public string WeightAsString
        {
            get {
                return string.Format("{0}lbs",weight);
            }
        }

        public string BuyDescription 
        {
            get {
                return string.Format("Would you like to buy {0} for {1}?", displayName, inGameCurrencyValue );
            }
        }

        public string AnimationState
        {
            get {
                return enabled ? "Active" : "Inactive";
            }
        }

		public override void DoAction ()
		{
            if (!purchased)
            {
                MenuManager.Instance.OpenPopup( "ItemSale_Popup" , this );
                return;
            }

            enabled = !enabled;
		}
	}
}
