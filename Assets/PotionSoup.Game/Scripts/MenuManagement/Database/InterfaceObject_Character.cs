﻿using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;
using PotionSoup.Input;

namespace PotionSoup.MenuManagement.GameLogic
{
    public class InterfaceObject_Character : InterfaceObject 
    {
        public CharacterType characterType;
        public Sprite panel;
    }
}
