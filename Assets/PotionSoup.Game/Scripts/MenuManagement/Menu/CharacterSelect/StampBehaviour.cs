﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PotionSoup.MenuManagement;
using PotionSoup.Dispatcher;
using UnityEngine.UI;

public class StampBehaviour : MonoBehaviour 
{
    public DOTweenAnimation stampAnimator;
    public RectTransform rightArrow;
    public RectTransform leftArrow;
    protected DOTweenAnimation _animator;
    protected Image _stampImage;

    protected PlayerID playerID = PlayerID.one; 

    private void Start ()
    {
        CharacterSelection selection = GetComponentInParent < CharacterSelection > ();
        playerID = selection.playerId;

        _animator = GetComponent < DOTweenAnimation > ();
        _stampImage = stampAnimator.GetComponent < Image > ();

        EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnSelect, null, OnSelect, false );

        EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnDeselect, null, OnDeselect, false );

        //transform.eulerAngles = new Vector3 (0f,0f, Random.Range(minAngle,maxAngle));
    }

    public void OnComplete ()
    {
        _animator.DOPlayById("exit");
        _stampImage.gameObject.SetActive(true);
        _stampImage.transform.localScale = Vector3.one;
        _stampImage.color = Color.white;
    }

    public void OnSelect ( IMessage message )
    {
        Hashtable datatable = message.Data as Hashtable;

        PlayerID id = (PlayerID)datatable [ "PlayerId" ];

        if ( id == playerID )
        {
            _animator.DORestartById("enter");

            rightArrow.GetComponent < DOTweenAnimation > ().DOPause();
            leftArrow.GetComponent < DOTweenAnimation > ().DOPause();

            rightArrow.DOLocalMoveX(10f, 0.1f).SetRelative(true);
            leftArrow.DOLocalMoveX(-10f,0.1f).SetRelative(true);
            rightArrow.GetComponent < Image > ().DOFade(0f,0.1f);
            leftArrow.GetComponent < Image > ().DOFade(0f,0.1f);
        }
    }

    public void OnDeselect ( IMessage message )
    {
        Hashtable datatable = message.Data as Hashtable;

        PlayerID id = (PlayerID)datatable [ "PlayerId" ];

        if ( id == playerID )
        {
            stampAnimator.DORestart();

            rightArrow.DOLocalMoveX(-10f, 0.1f).SetRelative(true);
            leftArrow.DOLocalMoveX(10f,0.1f).SetRelative(true);
            rightArrow.GetComponent < Image > ().DOFade(1f,0.1f);
            leftArrow.GetComponent < Image > ().DOFade(1f,0.1f).OnComplete(
            
                () => {
                    rightArrow.GetComponent < DOTweenAnimation > ().DOPlay();
                    leftArrow.GetComponent < DOTweenAnimation > ().DOPlay();

                }
            );
        }
    }


}
