﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PotionSoup.MenuManagement;
using PotionSoup.Dispatcher;

public class PhotoRotate : MonoBehaviour 
{
    protected DOTweenAnimation _animator;

    public float minAngle = -2f;
    public float maxAngle = 2f;

    private void Start ()
    {
        CharacterSelection selection = GetComponentInParent < CharacterSelection > ();

        _animator = GetComponent < DOTweenAnimation > ();
       
        EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnHorizontalRight, selection.playerId, OnHorizontalRight, true );
        EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnHorizontalLeft, selection.playerId, OnHorizontalLeft, true );
    }

    public void OnHorizontalLeft ( IMessage message )
    {
        _animator.DORestart();
    }

    public void OnHorizontalRight ( IMessage message )
    {
        _animator.DORestart();
    }
}
