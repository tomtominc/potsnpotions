﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PotionSoup.MenuManagement;
using PotionSoup.Dispatcher;

public class ArrowMovement : MonoBehaviour 
{
    protected DOTweenAnimation _animator;
    protected RectTransform _rectTransform;

    public DOTweenAnimation otherArrow;
    public bool subscribeRight;
    public bool subscribeLeft;

    public float forwardMove = 10f;
    public float backwardMove = -10f;
    public float forwardScaleY = 0.8f;
    public float backwardScaleY = 1f;
    public float duration = 0.3f;

    protected bool _canPlay = true;

    private void Start ()
    {
        CharacterSelection selection = GetComponentInParent < CharacterSelection > ();

        _animator = GetComponent < DOTweenAnimation > ();

        _rectTransform = GetComponent < RectTransform > ();

        if ( subscribeLeft )
            EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnHorizontalLeft, selection.playerId.ToString(), OnHorizontalSelect, true );

        if ( subscribeRight )
            EventDispatcher.Subscribe < CharacterSelectEvent > ( CharacterSelectEvent.OnHorizontalRight, selection.playerId.ToString(), OnHorizontalSelect, true );
    }

    public void OnHorizontalSelect ( IMessage message )
    {
        if ( !_canPlay ) return;

        _canPlay = false;

        _animator.DOPause();
        otherArrow.DOPause();

        Sequence forwardSequence = DOTween.Sequence();
        Sequence backwardSequence = DOTween.Sequence();

        Tweener forwardMovement = _rectTransform.DOLocalMoveX ( forwardMove, duration );
        forwardMovement.SetRelative(true);

        Tweener forwardScale = _rectTransform.DOScaleY ( forwardScaleY, duration );

        forwardSequence.Append( forwardMovement );
        forwardSequence.Insert( 0, forwardScale );

        backwardSequence.Append( _rectTransform.DOLocalMoveX ( backwardMove, duration ).SetRelative(true) );
        backwardSequence.Insert( 0, _rectTransform.DOScaleY ( backwardScaleY, duration ) );

        backwardSequence.Pause();

        backwardSequence.OnComplete( () => DoPlay() );

        forwardSequence.OnComplete ( () => backwardSequence.Play() );

        forwardSequence.Play();
    }

    public void DoPlay ()
    {
        _animator.DOPlay();
        otherArrow.DOPlay();
        _canPlay = true;
    }
}
