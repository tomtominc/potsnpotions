﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D.Tween
{
	/// <summary>
	/// Tween mode.
	/// </summary>
	public enum TweenMode
	{
		SNAP,
		LINEAR,
		EASE_IN_OUT
	}
}