﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Hang from the ceiling on collision.
	/// </summary>
	public class SpecialMovement_HangFromCeiling : SpecialMovement
	{
		/// <summary>
		/// How long we fall for after letting go of ceiling.
		/// </summary>
		public float fallTime = 0.1f;

		/// <summary>
		/// Difference between y position of jump and hang.
		/// </summary>
		public float yOffset = 0.2f;

		/// <summary>
		/// Have we started hanging?
		/// </summary>
		protected bool hangStarted;

		/// <summary>
		/// The fall timer.
		/// </summary>
		protected float fallTimer;


		#region constants
		
		/// <summary>
		/// Human readable name.
		/// </summary>
		private const string Name = "Ceiling Hang";
		
		/// <summary>
		/// Human readable description.
		/// </summary>
		private const string Description = "Hang from the ceiling on collision.";
		
		/// <summary>
		/// Static movement info used by the editor.
		/// </summary>
		new public static MovementInfo Info
		{
			get
			{
				return new MovementInfo(null, Description);
			}
		}
		#endregion

		/// <summary>
		/// Unity Update hook.
		/// </summary>
		void Update()
		{
			if (fallTimer > 0) fallTimer -= TimeManager.FrameTime;
		}
		
		/// <summary>
		/// Initialise this movement and retyrn a reference to the ready to use movement.
		/// </summary>
		/// <param name="character">Character.</param>
		override public Movement Init(Character character)
		{
			this.character = character;
			return this;
		}

		/// <summary>
		/// Initialise the movement with the given movement data.
		/// </summary>
		/// <param name="character">Character.</param>
		/// <param name="movementData">Movement data.</param>
		/// <param name="ignored">Ignored.</param>
		override public Movement Init(Character character, MovementVariable[] ignored)
		{
			return Init (character);
		}

		/// <summary>
		/// Gets a value indicating whether this movement wants to do a special move.
		/// </summary>
		/// <returns><c>true</c>, if special move was wanted, <c>false</c> otherwise.</returns>
		override public bool WantsSpecialMove()
		{
			if (fallTimer > 0) return false;
			if (hangStarted)
			{
				// Down to release
				if (character.Input.VerticalAxisDigital == -1) 
				{
					fallTimer = fallTime;
					return false;
				}
				return true;
			}
			else if ((character.PreviousVelocity.y >= -1.0f || character.Velocity.y >= 1.0f ) && CheckHeadCollisions())
			{
				return true;
			}
			return false;
		}
		
		/// <summary>
		/// Start the special mvoe
		/// </summary>
		override public void DoSpecialMove()
		{
			if (!hangStarted)
			{
				hangStarted = true;
				character.SetVelocityX (0);
				character.SetVelocityY (0);
				character.Translate(0, yOffset, true);
			}
		}

		/// <summary>
		/// Called when the movement loses control.
		/// </summary>
		override public void LosingControl()
		{
			hangStarted = false;
		}

		/// <summary>
		/// Gets the animation state that this movement wants to set.
		/// </summary>
		override public AnimationState AnimationState
		{
			get 
			{
				return AnimationState.ACRO_CUSTOM_0;
			}
		}

		/// <summary>
		/// This class will handle gravity internally.
		/// </summary>
		override public bool ShouldApplyGravity
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="PotionSoup.Engine2D.Movement"/> expects the
		/// base collisions to be executed after its movement finishes.
		/// </summary>
		override public RaycastType ShouldDoBaseCollisions
		{
			get
			{
				// We don't want to check head collissions so we don't unparent if parented to a moving platform 
				// if there is a chance that something can push you downwards while hanging from cieling you may want
				// to make this check a little more clever.
				return RaycastType.SIDES | RaycastType.FOOT;
				
			}
		}

		/// <summary>
		/// Checks the head collisions.
		/// </summary>
		/// <returns><c>true</c>, if head colliders were hitting something , <c>false</c> otherwise.</returns>
		virtual protected bool CheckHeadCollisions()
		{
			int hitCount = 0;
			for (int i = 0; i < character.Colliders.Length; i++)
			{
				if (character.Colliders[i].RaycastType == RaycastType.HEAD)
				{
					RaycastHit2D hit = character.GetClosestCollision(i);
					if (hit.collider != null)
					{
						hitCount++;
					}
				}
			}
			if (hitCount > 1) return true;
			return false;
		}
	}
}
