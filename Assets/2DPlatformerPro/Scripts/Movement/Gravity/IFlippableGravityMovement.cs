﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Interface implemented by movements which can handle flipped gravity.
	/// </summary>
	public interface IFlippableGravityMovement
	{

	}
}