using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Data for a movement variable
	/// </summary>
	[System.Serializable]
	public class MovementVariable
	{
		/// <summary>
		/// The float value.
		/// </summary>
		[SerializeField]
		protected float floatValue;

		/// <summary>
		/// The int value.
		/// </summary>
		[SerializeField]
		protected int intValue;

		/// <summary>
		/// The string value.
		/// </summary>
		[SerializeField]
		protected string stringValue;

		/// <summary>
		/// The curve value.
		/// </summary>
		[SerializeField]
		protected AnimationCurve curveValue;

		/// <summary>
		/// The bool value.
		/// </summary>
		[SerializeField]
		protected bool boolValue;

		
		/// <summary>
		/// The vector2 value.
		/// </summary>
		[SerializeField]
		protected Vector2 vector2Value;

		/// <summary>
		/// Gets or sets the float value.
		/// </summary>
		public float FloatValue
		{
			get
			{
				return floatValue;
			}
			set
			{
				floatValue = value;
			}
		}
		
		/// <summary>
		/// Gets or sets the int value.
		/// </summary>
		public int IntValue
		{
			get
			{
				return intValue;
			}
			set
			{
				intValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the string value.
		/// </summary>
		public string StringValue
		{
			get
			{
				return stringValue;
			}
			set
			{
				stringValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the curve value.
		/// </summary>
		public AnimationCurve CurveValue
		{
			get
			{
				return curveValue;
			}
			set
			{
				curveValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the bool value.
		/// </summary>
		public bool BoolValue
		{
			get
			{
				return boolValue;
			}
			set
			{
				boolValue = value;
			}
		}

		/// <summary>
		/// Gets or sets the vector2 value.
		/// </summary>
		public Vector2 Vector2Value
		{
			get 
			{
				return vector2Value;
			}
			set
			{
				vector2Value = value;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		public MovementVariable ()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		public MovementVariable (float floatValue)
		{
			this.floatValue = floatValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		public MovementVariable (bool boolValue)
		{
			this.boolValue = boolValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		public MovementVariable (string stringValue)
		{
			this.stringValue = stringValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		public MovementVariable (int intValue)
		{
			this.intValue = intValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		public MovementVariable (AnimationCurve curveValue)
		{
			this.curveValue = curveValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		/// <param name="vector2Value">Vector2 value.</param>
		public MovementVariable (Vector2 vector2Value)
		{
			this.vector2Value = vector2Value;
		}

		/// <summary>
		/// Clone constructor. Initializes a new instance of the <see cref="PotionSoup.Engine2D.MovementVariable"/> class.
		/// </summary>
		/// <param name="originalVariable">Variable to clone values from.</param>
		public MovementVariable (MovementVariable originalVariable)
		{
			if (originalVariable != null)
			{
				this.floatValue = originalVariable.floatValue;
				this.intValue = originalVariable.intValue;
				this.stringValue = originalVariable.stringValue;
				this.curveValue = originalVariable.curveValue;
				this.boolValue = originalVariable.boolValue;
				this.vector2Value = originalVariable.vector2Value;
			}
		}
		

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="PotionSoup.Engine2D.MovementVariable"/>.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="PotionSoup.Engine2D.MovementVariable"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
		/// <see cref="PotionSoup.Engine2D.MovementVariable"/>; otherwise, <c>false</c>.</returns>
		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType () != typeof(MovementVariable))
				return false;
			MovementVariable other = (MovementVariable)obj;
			return floatValue == other.floatValue && intValue == other.intValue && stringValue == other.stringValue && curveValue == other.curveValue && boolValue == other.boolValue &&  vector2Value.x == other.vector2Value.x && vector2Value.y == other.vector2Value.y ;
		}
		
		/// <summary>
		/// Serves as a hash function for a <see cref="PotionSoup.Engine2D.MovementVariable"/> object.
		/// </summary>
		/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
		public override int GetHashCode ()
		{
			unchecked
			{
				return floatValue.GetHashCode () ^ intValue.GetHashCode () ^ (stringValue != null ? stringValue.GetHashCode () : 0) ^ (curveValue != null ? curveValue.GetHashCode () : 0) ^ boolValue.GetHashCode () ^ (vector2Value.x.GetHashCode ())  ^ (vector2Value.y.GetHashCode ())  ;
			}
		}
		
	}
	
}