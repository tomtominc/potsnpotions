﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Represents different ways of storing velocity.
	/// </summary>
	public enum VelocityType
	{
		RELATIVE_X_WORLD_Y,
		RELATIVE,
		WORLD,
		CUSTOM
	}
}
