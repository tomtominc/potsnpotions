﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{

	/// <summary>
	/// This interface is used by movements that are physics based, it helps other movements decide what to do when transitioning control to them.
	/// </summary>
	public interface IPhysicsMovement
	{

	}
}