﻿/**
 * This code is part of Platformer PRO and is copyright John Avery 2014.
 */

using UnityEngine;
using System.Collections.Generic;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Stores details about the items a character has collected. Should be on the same 
	/// Game Object as the character.
	/// </summary>
	public class ItemManager : Persistable
	{

		/// <summary>
		/// Data about the available stackable types.
		/// </summary>
		public List<StackableItemData> collectibleItems;

		/// <summary>
		/// The character this item manager applies to.
		/// </summary>
		protected Character character;

		/// <summary>
		/// The item data.
		/// </summary>
		protected ItemData itemData;

		/// <summary>
		/// Have we loaded the data yet?
		/// </summary>
		protected bool loaded;
	
		/// <summary>
		/// The player preference identifier.
		/// </summary>
		public const string UniqueDataIdentifier = "ItemManagerData";

		#region events

		/// <summary>
		/// Item collected.
		/// </summary>
		public event System.EventHandler <ItemEventArgs> ItemCollected;

		/// <summary>
		/// Raises the item collected event.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="amount">Number collected.</param>
		/// <param name="character">Character.</param>
		virtual protected void OnItemCollected(ItemClass itemClass, string type, int amount, Character character)
		{
			if (saveOnChange) Save (this);
			if (ItemCollected != null)
			{
				ItemCollected(this, new ItemEventArgs(itemClass, type, amount, character));
			}
		}

		/// <summary>
		/// Item consumed.
		/// </summary>
		public event System.EventHandler <ItemEventArgs> ItemConsumed;
		
		/// <summary>
		/// Raises the item consumed event.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="amount">Number consumed.</param>
		/// <param name="character">Character.</param>
		virtual protected void OnItemConsumed(ItemClass itemClass, string type, int amount, Character character)
		{
			if (ItemConsumed != null)
			{
				ItemConsumed(this, new ItemEventArgs(itemClass, type, amount, character));
			}
		}

		/// <summary>
		/// Item collected.
		/// </summary>
		public event System.EventHandler <ItemEventArgs> ItemMaxUpdated;

		/// <summary>
		/// Raises the item max updated event.
		/// </summary>
		/// <param name="itemClass">Item class.</param>
		/// <param name="type">Type.</param>
		/// <param name="amount">Amount.</param>
		/// <param name="character">Character.</param>
		virtual protected void OnItemMaxUpdated(ItemClass itemClass, string type, int amount, Character character)
		{
			if (ItemMaxUpdated != null)
			{
				ItemMaxUpdated(this, new ItemEventArgs(itemClass, type, amount, character));
			}
		}

		#endregion

		/// <summary>
		/// Unity Awake() hook.
		/// </summary>
		void Awake()
		{
			if (character == null) character = GetComponent<Character>();
			if (character == null) character = GetComponentInParent<Character>();
		}

		/// <summary>
		/// Unity start hook.
		/// </summary>
		void Start()
		{
			Init ();
		}
		
		/// <summary>
		/// Init this instance.
		/// </summary>
		override protected void Init()
		{
			base.Init ();
			if (!loaded)
			{
				itemData = new ItemData();
				foreach (StackableItemData item in collectibleItems)
				{
					itemData.AddStackable(item.type, item.startingCount);
				}
				character = GetComponentInParent<Character>();
				if (character == null) Debug.LogError("An ItemManager must be on the same GameObject, or a child of, a Character");
				loaded = true;
				OnLoaded();
			}
		}

		/// <summary>
		/// Collect the given item
		/// </summary>
		/// <returns>The actual number collected.</returns>
		/// <param name="item">Item.</param>
		virtual public int CollectItem(Item item)
		{
			if (item.itemClass == ItemClass.STACKABLE) 
			{
				return DoCollectStackable(item);
			}
			else
			{
				return DoCollect (item);
			}
		}

		/// <summary>
		/// Sets the item count without raiing a collection event.
		/// </summary>
		/// <returns>The item count.</returns>
		/// <param name="itemType">Item type.</param>
		/// <param name="amount">Item count.</param>
		virtual public int SetItemCount(string itemType, int amount)
		{
			if (itemData.ContainsKey(itemType))
			{
				if (amount > ItemMax(itemType)) amount = ItemMax (itemType);
				itemData[itemType] = amount;
				return amount;
			}
			else
			{
				if (amount > ItemMax(itemType)) amount = ItemMax (itemType);
				itemData[itemType] = amount;
				itemData.AddStackable (itemType, amount);
				return amount;
			}
		}

		/// <summary>
		/// Gets the number of items of the given type.
		/// </summary>
		/// <returns>The count.</returns>
		/// <param name="itemType">Item type.</param>
		virtual public int ItemCount(string itemType)
		{
			if (itemData != null) if (itemData.ContainsKey(itemType)) return itemData[itemType];
			return 0;
		}

		/// <summary>
		/// Gets the maximum number of items for the given type.
		/// </summary>
		/// <returns>The count.</returns>
		/// <param name="itemType">Item type.</param>
		virtual public int ItemMax(string itemType)
		{
			foreach (StackableItemData stackableItem in collectibleItems)
			{
				if (stackableItem.type == itemType) return stackableItem.max;
			}
			// TODO Some kind of non collectible item?

			return 1;
		}

		/// <summary>
		/// Increases the maximum number of items for the given type and returns the new max.
		/// </summary>
		/// <returns>The count.</returns>
		/// <param name="itemType">Item type.</param>
		/// <param name="amount">Amont oadd to item  max.</param>
		virtual public int IncreaseItemMax(string itemType, int amount)
		{
			foreach (StackableItemData stackableItem in collectibleItems)
			{
				if (stackableItem.type == itemType)
				{
					stackableItem.max += amount;
					OnItemMaxUpdated(ItemClass.STACKABLE, itemType, stackableItem.max, character);
					return stackableItem.max;
				}
			}
			// TODO Some kind of non collectible item?
			
			return 1;
		}

		/// <summary>
		/// Sets the maximum number of items for the given type.
		/// </summary>
		/// <param name="itemType">Item type.</param>
		/// <param name="amount">New max item amount.</param>
		virtual public void SetItemMax(string itemType, int amount)
		{
			foreach (StackableItemData stackableItem in collectibleItems)
			{
				if (stackableItem.type == itemType)
				{
					stackableItem.max = amount;
					OnItemMaxUpdated(ItemClass.STACKABLE, itemType, stackableItem.max, character);
					return;
				}
			}
			// TODO Some kind of non collectible item?

		}

		/// <summary>
		/// Consumes the given amount of items of type itemType.
		/// </summary>
		/// <param name="itemType">Item type.</param>
		/// <param name="amount">Amount to consume.</param>
		/// <returns>The actual amount consumed.</returns>
		virtual public int ConsumeItem(string itemType, int amount)
		{
			// Stackable
			if (itemData.ContainsKey(itemType)) 
			{
				int actualAmount = itemData.Consume(itemType, amount);
				OnItemConsumed(itemData.TypeForKey(itemType), itemType, actualAmount, character);
				return actualAmount;
			}
			return 0;
		}
		
		/// <summary>
		/// Gets a list of items that the character has.
		/// </summary>
		/// <returns>The items the character has.</returns>///
		virtual public List<ItemAndCount> GetItems()
		{
			List<ItemAndCount> result = new List<ItemAndCount> ();
			foreach (string item in itemData.items)
			{
				result.Add(new ItemAndCount(item, 1));
			}
			for(int i = 0; i < itemData.stackableItemCountsIds.Count; i++)
			{
				result.Add(new ItemAndCount( itemData.stackableItemCountsIds[i],  itemData.stackableItemCountsCounts[i]));
			}
			return result;
		}

		/// <summary>
		/// Returns true if the character has at least one of the given item.
		/// </summary>
		/// <param name="itemType">Item type.</param>
		/// <returns>True if the character has the item, false otherwise.</returns>
		virtual public bool HasItem(string itemType)
		{
			return itemData.ContainsKey(itemType);
		}

		/// <summary>
		/// Handle collecting a stackable item.
		/// </summary>
		/// <returns>The actual number collected.</returns>
		/// <param name="item">Item.</param>
		virtual protected int DoCollectStackable(Item item) 
		{
			int amount = 1;
			if (item is StackableItem) amount = ((StackableItem)item).amount;
			int max =  ItemMax(item.type);
			if (itemData.ContainsKey(item.type))
		    {
				if (itemData[item.type] + amount > ItemMax(item.type))
				{
					int remainder = amount - (max - itemData[item.type]);
					itemData[item.type] = max;
					OnItemCollected (item.itemClass, item.type, remainder, character);
					return remainder;
				}
				itemData[item.type] += amount;
				OnItemCollected (item.itemClass, item.type, amount, character);
				return amount;
			}
			else
			{
				if (itemData[item.type] + amount > ItemMax(item.type))
				{
					int remainder = amount - max;
					itemData.AddStackable (item.type, max);
					OnItemCollected (item.itemClass, item.type, remainder, character);
					return remainder;
				}
				itemData.AddStackable (item.type, amount);
				OnItemCollected (item.itemClass, item.type, amount, character);
				return amount;
			}

		}

		/// <summary>
		/// Handle collecting a non-stackable item.
		/// </summary>
		/// <returns>One if the item was collected or 0 if we already have this item.</returns>
		/// <param name="item">Item.</param>
		virtual protected int DoCollect(Item item) 
		{
			if (item.itemClass != ItemClass.NONE)
			{
				if (itemData.ContainsKey(item.type)) return 0;
				itemData.AddNonStackable (item.type);
			}
			OnItemCollected (item.itemClass, item.type, 1, character);
			return 1;
		}

		#region Persitable methods

		/// <summary>
		/// Gets the character reference.
		/// </summary>
		/// <value>The character.</value>
		override public Character Character
		{
			get
			{
				return character;
			}
		}

		/// <summary>
		/// Gets the data to save.
		/// </summary>
		override public object SaveData
		{
			get
			{
				return itemData;
			}
		}
		
		/// <summary>
		/// Get a unique identifier to use when saving the data (for example this could be used for part of the file name or player prefs name).
		/// </summary>
		/// <value>The identifier.</value>
		override public string Identifier
		{
			get
			{
				return UniqueDataIdentifier;
			}
		}
		
		/// <summary>
		/// Applies the save data to the object.
		/// </summary>
		override public void ApplySaveData(object t)
		{
			if (t is ItemData)
			{
				this.itemData = (ItemData)t;
				loaded = true;
				OnLoaded();
			}
			else Debug.LogError("Tried to apply unepxected data: " + t.GetType());
		}
		
		/// <summary>
		/// Get the type of object this Persistable saves.
		/// </summary>
		override public System.Type SavedObjectType()
		{
			return typeof(ItemData);
		}

		/// <summary>
		/// Resets the save data back to default.
		/// </summary>
		override public void ResetSaveData()
		{
			itemData = new ItemData ();
			foreach (StackableItemData item in collectibleItems)
			{
				itemData.AddStackable(item.type, item.startingCount);
			}
#if UNITY_EDITOR
			Save(this);
#endif
		}

		#endregion
	}
}