﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Animation transition mapping. Relates a state and previous state.
	/// </summary>
	[System.Serializable]
	public class AnimationTransitionMapping
	{
		[Tooltip ("The state we are moving from")]
		public AnimationState previousState;

		[Tooltip ("The state we are moving to")]
		public AnimationState newState;
	}
}
