﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Provides an axis via a touch input.
	/// </summary>
	public interface ITouchAxis
	{
		/// <summary>
		/// Gets the axis value.
		/// </summary>
		float Value { get; }
	}
}
