using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
	/// <summary>
	/// Classifies different attack types.
	/// </summary>
	public enum AttackType
	{
		MELEE,
		PROJECTILE
	}
}

