using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{

	/// <summary>
	/// Projectile used in projectile (ranged) attacks that is controlled by physics.
	/// </summary>
	public class GrenadeProjectile : Projectile
	{
		/// <summary>
		/// How long after hitting something until we explode.
		/// </summary>
		public float triggerDelay;

		/// <summary>
		/// How long after exploding is the hit box active.
		/// </summary>
		public float hitBoxLifeTime;

		/// <summary>
		/// When the grenade explodes, this is the hit box.
		/// </summary>
		public GameObject grenadeHurtBox;

		/// <summary>
		/// Has greande triggered?
		/// </summary>
		protected bool triggered;

		/// <summary>
		/// Character reference.
		/// </summary>
		protected IMob character;
		
		/// <summary>
		/// Reference to the rigidbody 2D.
		/// </summary>
		new protected Rigidbody2D rigidbody2D;

		/// <summary>
		/// Unity Update hook.
		/// </summary>
		void Update() {
			// Override update, no need to move, rigidbody will handle it
		}

		/// <summary>
		/// Call to start the projectile moving.
		/// </summary>
		/// <param name="damageAmount">Damage amount.</param>
		/// <param name="damageType">Damage type.</param>
		override public void Fire(int damageAmount, DamageType damageType, Vector2 direction, IMob character) 
		{
			rigidbody2D = GetComponent<Rigidbody2D> ();
			fired = true;
			damageInfo = new DamageInfo(damageAmount, damageType, Vector2.zero, character);
			rigidbody2D.velocity = (new Vector2(0,1) + direction) * speed;
			this.character = character;
		}

		/// <summary>
		/// Destroy projectile.
		/// </summary>
		override public void DestroyProjectile(bool isEnemyHit)
		{
			fired = false;
			StartCoroutine(DoDestroy(isEnemyHit));
		}

		/// <summary>
		/// Delays then sends event and enables hit box.
		/// </summary>
		/// <returns>The trigger.</returns>
		virtual protected IEnumerator DoTrigger()
		{
			yield return new WaitForSeconds(triggerDelay);
			GameObject go = (GameObject) GameObject.Instantiate(grenadeHurtBox);
			ProjectileHitBox projectileHitBox = go.GetComponent<ProjectileHitBox>();
			projectileHitBox.Init (damageInfo, character, this, false, false);
			this.projectileHitBox = projectileHitBox;
			projectileHitBox.enabled = true;
			go.transform.position = transform.position;
			rigidbody2D.isKinematic = true;
			rigidbody2D.gravityScale = 0;
			OnProjectileDestroyed(damageInfo);
			yield return new WaitForSeconds(hitBoxLifeTime);
			Destroy(go);
			DestroyProjectile(false);
		}
		
		/// <summary>
		/// Unity collider trigger. Start the explosiion coroutine.
		/// </summary>
		void OnCollisionEnter2D (Collision2D info) 
		{
			if (!triggered) 
			{
				triggered = true;
				StartCoroutine(DoTrigger());
			}
		}
	}

}