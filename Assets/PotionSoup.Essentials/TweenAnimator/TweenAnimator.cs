﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using DG.Tweening;
using System.Linq;

public class TweenAnimator : MonoBehaviour
{
    [HideInInspector][SerializeField]
    public List < SequenceMotion > sequences = new List < SequenceMotion >();

    [HideInInspector]
    public int playOnAwakeSequence = -1;

    [HideInInspector]
    public bool playSequenceOnStart = false;

    private void Start ()
    {
        if ( playSequenceOnStart && playOnAwakeSequence > -1 && playOnAwakeSequence <  sequences.Count )
        {
            sequences[playOnAwakeSequence].Play(gameObject);
        }
    }

    public bool Add ( string key )
    {
        if (ContainsKey(key) == false)
        {
            SequenceMotion sequence = new SequenceMotion( key );
            sequences.Add(sequence);

            return true;
        }

        return false;
    }
    public void Add(string key, TweenMotion motion)
    {
        SequenceMotion sequence = GetValue(key);

        if ( sequence == null )
        {
            sequence = new SequenceMotion( key );
            sequence.Add(motion);
            sequences.Add(sequence);
        }
        else 
        {
            sequence.Add(motion);
        }
    }

    public bool Add (string key, SequenceMotion sequence )
    {
        if (ContainsKey(key) == false)
        {
            sequences.Add(sequence);
            return true;
        }

        return false;
    }

    public SequenceMotion GetValue ( string key )
    {
        SequenceMotion sequence;

        if ( TryGetValue(key, out sequence) )
        {
            return sequence;
        }

        return null;
    }

    public bool ContainsKey ( string key )
    {
        var list = sequences.Where ( x => x.key == key).ToList();

        if ( list.Count < 1 )
        {
            return false;
        }

        return true;
    }

    public bool TryGetValue ( string key, out SequenceMotion sequence)
    {
        sequence = sequences.Where ( x => x.key == key).FirstOrDefault();

        if ( sequence == null )
        {
            return false;
        }
        else 
        {
            return true;
        }
       
    }

    public int Count 
    {
        get { return sequences.Count; }
    }

    public string[] Keys
    {
        get {

            return sequences.ConvertAll( x => x.key ).ToArray();
        }
    }

}
