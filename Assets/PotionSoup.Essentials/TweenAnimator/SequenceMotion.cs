﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.Events;

[System.Serializable]
public class SequenceMotion
{
    public enum CullingType
    {
        KeepCurrent,
        RenderInBack,
        RenderInFront
    }

    [SerializeField]
    public string key = string.Empty;

    [SerializeField]
    public CullingType cullingType = CullingType.KeepCurrent;

    [SerializeField]
    public List < TweenMotion > tweens = new List < TweenMotion >();

    [SerializeField]
    public List < TweenCustomEvent > CustomEvents = new List < TweenCustomEvent >();

    public void Add(TweenMotion motion)
    {
        tweens.Add(motion); 
    }

    public SequenceMotion(string key)
    {
        this.key = key;
    }

    public Sequence Play( GameObject user )
    {
        Sequence sequence = DOTween.Sequence();

        foreach ( var e in CustomEvents )
        {
            sequence.InsertCallback(e.atTime,() => e.OnAction.Invoke());
        }
         
        foreach ( var tween in tweens )
        {
            Tweener tweener = tween.Build(user);

            if (tweener == null) continue;

            tweener.Play();
            //sequence.Insert(0,tweener);
        }

        return sequence.Play();
    }

}

[System.Serializable]
public class TweenEvent : UnityEvent < TweenMotion >
{
}

[System.Serializable]
public class TweenCustomEvent
{
    [SerializeField]
    public string name;
    [SerializeField]
    public float atTime = 0f;
    [SerializeField]
    public UnityEvent OnAction = new UnityEvent();
}

