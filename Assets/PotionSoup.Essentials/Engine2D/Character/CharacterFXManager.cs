﻿using UnityEngine;
using System.Collections;
using PotionSoup.Dispatcher;

namespace PotionSoup.Engine2D
{
    public class CharacterFXManager : MonoBehaviour
    {
        public CharacterManager character { get; private set; }

        private void Start()
        {
            character = GetComponent < CharacterManager >();

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnRun, character.playerId, OnRunFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnRunDirectionChange, character.playerId, OnRunDirectionChangeFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnSlide, character.playerId, OnSlideFX, false);

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnJump, character.playerId, OnJumpFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnDoubleJump, character.playerId, OnDoubleJumpFX, false);

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnWallSlide, character.playerId, OnWallSlideFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnGroundPound, character.playerId, OnGroundPoundFX, false);

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnBuyItem, character.playerId, OnBuyItemFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnCollectItem, character.playerId, OnCollectItemFX, false);

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnDamage, character.playerId, OnDamageFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnDeath, character.playerId, OnDeathFX, false);

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnSetVulnerable, character.playerId, OnSetVulnerableFX, false);
            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnSetInvulnerable, character.playerId, OnSetInvulnerableFX, false);

            EventDispatcher.Subscribe< CharacterEvent >(CharacterEvent.OnScreenWrap, character.playerId, OnScreenWrapFX, false);
        }

        private void OnRunFX (IMessage message)
        {
            Debug.LogWarning("OnRunFX Not Implemented.");
        }

        private void OnRunDirectionChangeFX (IMessage message)
        {
            Debug.LogWarning("OnRunDirectionChangeFX Not Implemented.");
        }

        private void OnSlideFX (IMessage message)
        {
            Debug.LogWarning("OnSlideFX Not Implemented.");
        }

        private void OnWallSlideFX (IMessage message)
        {
            Debug.LogWarning("OnWallSlideFX Not Implemented.");
        }

        private void OnGroundPoundFX (IMessage message)
        {
            Debug.LogWarning("OnGroundPoundFX Not Implemented.");
        }

        private void OnCollectItemFX (IMessage message)
        {
            Debug.LogWarning("OnCollectItemFX Not Implemented.");
        }

        private void OnBuyItemFX (IMessage message)
        {
            Debug.LogWarning("OnBuyItemFX Not Implemented.");
        }

        private void OnJumpFX(IMessage message)
        {
            Debug.LogWarning("OnJumpFX Not Implemented.");
        }

        private void OnDoubleJumpFX(IMessage message)
        {
            Debug.LogWarning("OnDoubleJumpFX Not Implemented.");
        }

        private void OnSetVulnerableFX(IMessage message)
        {
            Debug.LogWarning("OnSetVulnerableFX Not Implemented.");
        }

        private void OnSetInvulnerableFX(IMessage message)
        {
            Debug.LogWarning("OnSetInvulnerableFX Not Implemented.");
            }

        private void OnDamageFX(IMessage message)
        {
            Debug.LogWarning("OnDamageFX Not Implemented.");
        }

        private void OnDeathFX(IMessage message)
        {
            Debug.LogWarning("OnDamageFX Not Implemented.");
        }

        private void OnScreenWrapFX(IMessage message)
        {
            Debug.LogWarning("OnScreenWrapFX Not Implemented.");
        }

    }
}