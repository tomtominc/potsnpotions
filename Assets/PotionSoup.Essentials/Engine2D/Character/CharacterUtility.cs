﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using PotionSoup.GameLogic;
using PotionSoup.PrefabManagment;

namespace PotionSoup.Engine2D
{
	public class CharacterUtility : Singleton < CharacterUtility >  
	{
		protected static Dictionary < PlayerID, CharacterManager > characters 
        = new Dictionary< PlayerID, CharacterManager > ();

		
		public static void SpawnCharacter ( CharacterData data )
		{
			Transform prefab = SimplePrefabManager.Get(StringManager.prefab_character).transform;

			Vector3 spawnPoint = Vector3.zero; // TODO: Create a function to Get a spawn point with playerID

			Transform clone = (Transform)Instantiate( prefab , spawnPoint , Quaternion.identity );
			clone.name = "Player_" + data.type.ToString() + "_" + data.playerId.ToString();
			
			CharacterManager bridge = clone.GetComponent < CharacterManager > ();
			
			bridge.SetData(data);

			AddCharacter ( data.playerId , bridge );
		}


		public static void AddCharacter ( PlayerID playerId, CharacterManager character )
		{
			if (characters.ContainsKey(playerId)) Debug.LogError ("Character by the playerId already added, try adding another one" );

			characters.Add(playerId,character);
		}

        public static CharacterManager GetCharacter ( PlayerID playerId )
		{
			if (!characters.ContainsKey (playerId)) Debug.LogError ("Character by the playerId does not exist" );

			return characters[playerId];
		}
			
		public static int GetValue ( PlayerID playerId )
		{
			if (!characters.ContainsKey(playerId)) return 0;
			CharacterData data = characters[ playerId ].Data;
			return data.value;
		}
			
		public static CharacterData[] GetCharacterData ()
		{
			List<CharacterData> datas = new List<CharacterData>();
			string suffix = StringManager.key_character_save;
			foreach (PlayerID id in Enum.GetValues(typeof(PlayerID)))
			{
				CharacterData data = DataBridge.GetValue<CharacterData>(id.ToString() + suffix, null);
				
				if (data == null) continue;
				
				if (data.playable == true)
				{
					datas.Add(data);
				}
			}

			return datas.ToArray();
		}
		
		public static int CurrentlyActiveCharacterCount 
		{
			get {
				if (characters.Count <= 0)
				{
					return -1;
				}
				
				int aliveCount = CurrentlyActiveCharacters.Length;
				
				return aliveCount;
			}
		}
		
        public static CharacterManager[] CurrentlyActiveCharacters
		{
			get 
			{
                List<CharacterManager> alive = new List<CharacterManager>();
				
				foreach ( var pair in characters )
				{
					if ( pair.Value.Health() > 0 )
					{
						if (!alive.Contains(pair.Value)) alive.Add (pair.Value);
					}
				}
				return alive.ToArray();
			}
		}
	}
}
