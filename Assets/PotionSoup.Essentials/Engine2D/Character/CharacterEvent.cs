﻿using UnityEngine;
using System.Collections;

public enum CharacterEvent  
{
    OnRun,
    OnRunDirectionChange,
    OnSlide,

    OnJump,
    OnDoubleJump,

    OnWallSlide,
    OnGroundPound,

    OnBuyItem,
    OnCollectItem,

    OnDamage,
    OnDeath,

    OnSetVulnerable,
    OnSetInvulnerable,
    OnScreenWrap
}
