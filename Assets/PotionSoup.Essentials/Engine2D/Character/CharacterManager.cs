﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{

    public class CharacterManager : MonoBehaviour
    {
        public PlayerID playerId = PlayerID.one;

        public CharacterData Data { get; private set; }

        public Character Character { get; private set; }

        public CharacterHealth CharacterHealth { get; private set; }

        private void Awake()
        {
            Character = GetComponent < Character >(); 
            CharacterHealth = GetComponent < CharacterHealth >(); 
        }

        public virtual void SetData(CharacterData data)
        {
            Data = data;    
        }

        public virtual void SetVulnerable()
        {
            CharacterHealth.SetVulnerable();
        }

        public virtual void SetInvulnerable()
        {
            CharacterHealth.SetInvulnerable();
        }

        public virtual int Health()
        {
            return CharacterHealth.CurrentHealth;
        }

        public virtual void SaveGameData()
        {
            
        }
    }
}
