﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
    public enum CharacterType { Green, Blue, Purple, Red }
   
    [System.Serializable]
    public class CharacterData
    {
        public PlayerID playerId;
        public CharacterType type;
		public int value;
		public bool playable = true;

        public CharacterData(){ }

		public CharacterData (PlayerID playerId, CharacterType type)
        {
            this.playerId = playerId;
            this.type = type;
        }
    }
}