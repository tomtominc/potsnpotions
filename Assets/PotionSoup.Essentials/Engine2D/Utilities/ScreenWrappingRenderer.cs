﻿using UnityEngine;
using System.Collections;
using PotionSoup.Dispatcher;

namespace PotionSoup.Engine2D
{
	public class ScreenWrappingRenderer : MonoBehaviour 
	{
		private bool isWrappingX = false;
		private bool isWrappingY = false;
		private Camera mainCamera;
		private Transform root { get { return transform.root; } }
		private CharacterManager character;
       
		private void Start ()
		{
			mainCamera = Camera.main;
			character = root.GetComponent < CharacterManager > ();
		}

		private void OnBecameInvisible ()
		{
			ScreenWrap ();
		}

		private void OnBecameVisible ()
		{
			isWrappingX = false;
			isWrappingY = false;
		}

		private void ScreenWrap()
		{	
			
			if (!mainCamera || !root || ( isWrappingX && isWrappingY )) 
			{
				return;
			}
         
            EventDispatcher.Publish < CharacterEvent > ( this, CharacterEvent.OnScreenWrap, character.playerId , null );
			
			var newPosition = root.position;
			var viewportPosition = mainCamera.WorldToViewportPoint(newPosition);
			

			if (!isWrappingX && (viewportPosition.x > 1 || viewportPosition.x < 0))
			{
				newPosition.x = -newPosition.x;
				isWrappingX = true;
			}
			if (!isWrappingY && (viewportPosition.y > 1 || viewportPosition.y < 0))
			{
				newPosition.y = -newPosition.y;
				isWrappingY = true;
			}

			//Apply new position
			root.position = newPosition;
			
		}

	}
}
