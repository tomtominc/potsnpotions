﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PotionSoup.Engine2D
{
	public class Inventory : MonoBehaviour 
	{
		[System.Serializable]
		public class InventoryItem 
		{
			public string key;
			public int count;
		}
		public CharacterManager CharacterBridge { get; private set; }
		private Dictionary < string , int > Items = new Dictionary<string, int> ();
		public List< InventoryItem > startItems = new List < InventoryItem >();

		public void Initialize () 
		{
            CharacterBridge = GetComponent < CharacterManager > ();

			foreach (var item in startItems)
			{
				Add(item.key,item.count);
			}
		}

		public void Start () {}

		public void Add ( Item item )
		{
			string name = item.name;
            Add(name, 1);
            OnChangedInventory();
		}

		public void Add ( string name , int count )
		{
			
			if ( !Items.ContainsKey ( name ))
			{
				Items.Add ( name, 0 );
			}
			
			Items [ name ] += count;

            OnChangedInventory();
		}

		public int Count ( string name )
		{
			if ( !Items.ContainsKey ( name ))
			{
				return 0;
			}

			return Items[name];
		}

        public void Remove(string name, int count)
        {

        }

		public void RemoveAll ( string name )
		{
			if ( !Items.ContainsKey ( name ))
			{
				return;
			}
			Items[name] = 0;

            OnChangedInventory();
		}

        public Dictionary<string,int> GetInventory ()
        {
            return Items;
        }

        public void OnChangedInventory ()
        {
            CharacterBridge.SaveGameData();
        }

		public override string ToString ()
		{
			string itemList = "NOTHING IN YOUR INVENTORY";

			foreach ( var item in Items )
			{
				itemList += string.Format ("ITEM: {0} COUNT: {1}\n", item.Key , item.Value);
			}

			return itemList;
		}
	}
}
