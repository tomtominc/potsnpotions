﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Engine2D
{
    /// <summary>
    /// Defines an item that inherits from the character
    /// This is so it has all the properties of a character.
    /// </summary>
    public class ItemBase : Character 
    {
        
    }
}