﻿using UnityEngine;
using System.Collections;

public enum GameEvent 
{
    OnPaused,
    OnUnPaused,
}

