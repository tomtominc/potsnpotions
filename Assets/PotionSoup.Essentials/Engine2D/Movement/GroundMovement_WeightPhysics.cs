﻿using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;

using AnimationState = PotionSoup.Engine2D.AnimationState;

namespace PotionSoup.GameLogic
{
	public class GroundMovement_WeightPhysics : GroundMovement, IPhysicsMovement
	{

		public float acceleration = 20f;
		public float weight = 10f;
		public float maxSpeed = 8f;
		public float friction = 5f;
		public float quiesceSpeed = 0.5f;
		public float ignoreForce = 2f;
		public bool stickToGround = true;
		protected bool hasAppliedForce;

		override public bool SupportsSlidingOnSlopes
		{
			get
			{
				return true;
			}
		}
		private void Start () {}

		override public void DoMove()
		{
			if (stickToGround) SnapToGround ();

			float actualFriction = character.Friction;

			if (actualFriction == -1) actualFriction = friction;


			float actualAccel = acceleration - weight;

			// Apply drag
			if (character.Velocity.x > 0) 
			{
				character.AddVelocity(-character.Velocity.x * actualFriction * TimeManager.FrameTime, 0, false);
				if (character.Velocity.x < 0) character.SetVelocityX(0);
			}
			else if (character.Velocity.x < 0) 
			{
				character.AddVelocity(-character.Velocity.x * actualFriction * TimeManager.FrameTime, 0, false);
				if (character.Velocity.x > 0) character.SetVelocityX(0);
			}

			// Apply acceleration
			if (Mathf.Abs (character.Input.HorizontalAxis * actualAccel) > ignoreForce)
			{
				character.AddVelocity( (float)character.Input.HorizontalAxis * actualAccel * TimeManager.FrameTime, 0, false);
				hasAppliedForce = true;
			}

			// Limit to max speed
			if (character.Velocity.x > maxSpeed) 
			{
				character.SetVelocityX(maxSpeed);
			}
			else if (character.Velocity.x < -maxSpeed) 
			{
				character.SetVelocityX(-maxSpeed);
			}

			// Quiesce if moving very slowly and no force applied
			if (!hasAppliedForce)
			{
				if (character.Velocity.x > 0 && character.Velocity.x < quiesceSpeed ) 
				{
					character.SetVelocityX(0);
				}
				else if (character.Velocity.x  < 0 && character.Velocity.x > -quiesceSpeed)
				{
					character.SetVelocityX(0);
				}
			}

			// Translate
			character.Translate(character.Velocity.x * TimeManager.FrameTime, 0, false);

			hasAppliedForce = false;
		}

		override public Movement Init(Character character)
		{
			this.character = character;
			return this;
		}

		override public Movement Init(Character character, MovementVariable[] movementData)
		{
			return Init(character);
		}

		override public AnimationState AnimationState
		{
			get 
			{
				if (character.Input.HorizontalAxisDigital == 0)
				{
					if (character.Velocity.x != 0)
					{
						return AnimationState.SLIDE;
					}
					return AnimationState.IDLE;
				}
				else
				{
					if ((character.Input.HorizontalAxisDigital == 1 && character.Velocity.x < 0) ||
						(character.Input.HorizontalAxisDigital == -1 && character.Velocity.x > 0))
					{
						return AnimationState.SLIDE_DIR_CHANGE;
					}
					return AnimationState.WALK;
				}
			}
		}
		override public int FacingDirection
		{
			get 
			{
				return character.Input.HorizontalAxisDigital;
			}
		}

	}
}
