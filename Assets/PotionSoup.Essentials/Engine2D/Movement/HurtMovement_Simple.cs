﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PotionSoup.Dispatcher;

namespace PotionSoup.Engine2D
{
	public class HurtMovement_Simple : DamageMovementBase 
	{
		public AnimationState hurtState;
		public AnimationState deathState;

		protected AnimationState _currentState;
		protected bool _playing = false;
		protected Animator _animator;
		protected SpriteRenderer _renderer;

		override public void DoMove()
		{
			// Check if animation is finished
			AnimatorStateInfo info = _animator.GetCurrentAnimatorStateInfo(0);
			if (info.IsName(_currentState.AsString()))
			{
				if( info.normalizedTime >= 1.0f)
				{
					_playing = false;
					if (!isDeath) StartCoroutine(Blink());

                    Hashtable datatable = new Hashtable ()
                    {
                        {"DamageInfo", damageInfo},
                        {"IsDeath", isDeath}
                    };

                    EventDispatcher.Publish < CharacterEvent > ( this,  CharacterEvent.OnDamage, CharacterBridge.playerId, datatable );
				}
			}
		}
		override public Movement Init(Character character)
		{
			this.character = character;
			_animator = this.character.GetComponentInChildren<Animator> ();
			_renderer = _animator.GetComponent<SpriteRenderer>();
			return this;
		}
		override public Movement Init(Character character, MovementVariable[] movementData)
		{
			return Init(character);
		}
		override public bool WantsControl()
		{
			return _playing;
		}

		override public bool ShouldApplyGravity
		{
			get
			{
				return false;
			}
		}
		override public AnimationState AnimationState
		{
			get 
			{
				if (isDeath)
				{
					return deathState;
				}

				return hurtState;
			}
		}

		protected virtual IEnumerator Blink ()
		{
			//play a blink tween
			yield return new WaitForSeconds(3f);
			//kill the blink tween

			_renderer.color = new Color (_renderer.color.r,
				_renderer.color.g,_renderer.color.b,1f);
			
            CharacterBridge.SetVulnerable ();

		}

		override public void Damage(DamageInfo info, bool isDeath)
		{
			this.isDeath = isDeath;
			damageInfo = info;

			_currentState = isDeath ? deathState : hurtState;

			_playing = true;

            CharacterBridge.SetInvulnerable ();

			character.SetVelocityX(0);
			character.SetVelocityY(0);
		}

	}
}
