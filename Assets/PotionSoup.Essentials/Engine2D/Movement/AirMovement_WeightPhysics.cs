﻿using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;

using AnimationState = PotionSoup.Engine2D.AnimationState;
using PotionSoup.Dispatcher;

namespace PotionSoup.Engine2D
{
	public class AirMovement_WeightPhysics : AirMovementBase, IPhysicsMovement 
	{
		public float jumpVelocity;
		public float maxSpeed;
		public float drag;
		public bool useAnalogueInput;
		public float jumpGroundedLeeway;
		public float ignoreForce;
		public bool canDoubleJump;
		public float doubleJumpVelocity;
		public float airSpeed = 2f;
		public float friction = 1f;
		public float acceleration = 20f;
		public float weight = 0f;
		public float quiesceSpeed = 0.5f;

		protected bool hasAppliedForce = false;
		protected float readyToJumpTimer;
		protected bool jumpStart;
		protected bool showJumpStartedAnimation;
		protected bool hasRecievedCounterInput;
		protected int initialDirection;
		protected int jumpCount;
		protected bool jumpWhenButtonHeld;


		private void Start () {}

		override public AnimationState AnimationState
		{
			get 
			{
				if (showJumpStartedAnimation)
				{
					showJumpStartedAnimation = false;
					if (jumpCount > 1) return AnimationState.DOUBLE_JUMP;
					return AnimationState.JUMP;
				}
				else if (character.Velocity.y >= 0)
				{
					return AnimationState.AIRBORNE;
				}
				else
				{
					return AnimationState.FALL;
				}
			}
		}

		override public int AnimationPriority
		{
			get 
			{
				if (showJumpStartedAnimation)
				{
					return 5;
				}
				else if (character.Velocity.y >= 0)
				{
					return 0;
				}
				else
				{
					return 0;
				}
			}
		}
			
		override public int FacingDirection
		{
			get 
			{
				return character.Input.HorizontalAxisDigital;
			}
		}
			
		override public VelocityType VelocityType
		{
			get
			{
				return VelocityType.WORLD;
			}
		}

		override public bool ShouldDoRotations {
			get
			{
				return false;
			}
		}

		override public bool WantsJump()
		{
			if (!enabled) return false;
			// Pressing jump and on the ground or on a ladder (if the ladder wont allow us to jump then the ladder will retain control).
			if (((jumpCount == 0 && character.TimeSinceGroundedOrOnLadder <= jumpGroundedLeeway) || (jumpCount < 2 && canDoubleJump)) && 
				character.Input.JumpButton == ButtonState.DOWN && !jumpStart)
			{
				return true;
			}
			return false;
		}

		override public void DoMove()
		{
			DoRotation ();
			// If we left the ground move state to JUMPING
			if (jumpStart) 
			{
				// Jump has started once we leave the ground OR if we have hit our head (i.e. velocity back to zero or less)
				if (character.Velocity.y <= 0 || character.TimeSinceGroundedOrOnLadder > character.groundedLeeway) jumpStart = false;
				showJumpStartedAnimation = true;
			}
			MoveInX(character.Input.HorizontalAxis , character.Input.HorizontalAxisDigital, character.Input.RunButton);
			MoveInY();
		}

		override public Movement Init(Character character)
		{
			this.character = character;
			return this;
		}
		override public Movement Init(Character character, MovementVariable[] movementData)
		{
			return Init(character);
		}

		override public bool WantsControl()
		{
			if (!enabled) return false;
			if (jumpStart) return true;
			return false;
		}

		override public void GainControl()
		{
			if (character.ActiveMovement.VelocityType == VelocityType.RELATIVE_X_WORLD_Y)
			{
				Vector2 newVelocity = Quaternion.Euler(0,0, -character.PreviousRotation) * character.Velocity;
				character.SetVelocityX(newVelocity.x);
				// TODO Make this a constant and think about it a bit more
				character.SetVelocityY(newVelocity.y / 1.2f);
			}
			else if (character.ActiveMovement.VelocityType == VelocityType.WORLD)
			{
				// Same as we use, no need to change
			}
			hasRecievedCounterInput = false;

			if (character.Velocity.x > 0) initialDirection = 1;
			else if (character.Velocity.x < 0) initialDirection = -1;
			else initialDirection = 0;
		}

		override public void LosingControl()
		{
			jumpCount = 0;
			jumpStart = false;
		}

		override public bool ShouldApplyGravity
		{
			get
			{
				return false;
			}
		}

		override protected void MoveInX (float horizontalAxis, int horizontalAxisDigital, ButtonState runButton)
		{
			float actualFriction = character.Friction;

			if (actualFriction == -1) actualFriction = friction;


			float actualAccel = acceleration - weight;

			// Apply drag
			if (character.Velocity.x > 0) 
			{
				character.AddVelocity(-character.Velocity.x * actualFriction * TimeManager.FrameTime, 0, false);
				if (character.Velocity.x < 0) character.SetVelocityX(0);
			}
			else if (character.Velocity.x < 0) 
			{
				character.AddVelocity(-character.Velocity.x * actualFriction * TimeManager.FrameTime, 0, false);
				if (character.Velocity.x > 0) character.SetVelocityX(0);
			}

			// Apply acceleration
			if (Mathf.Abs (character.Input.HorizontalAxis * actualAccel) > ignoreForce)
			{
				character.AddVelocity( (float)character.Input.HorizontalAxis * actualAccel * TimeManager.FrameTime, 0, false);
				hasAppliedForce = true;
			}

			// Limit to max speed
			if (character.Velocity.x > maxSpeed) 
			{
				character.SetVelocityX(maxSpeed);
			}
			else if (character.Velocity.x < -maxSpeed) 
			{
				character.SetVelocityX(-maxSpeed);
			}

			// Quiesce if moving very slowly and no force applied
			if (!hasAppliedForce)
			{
				if (character.Velocity.x > 0 && character.Velocity.x < quiesceSpeed ) 
				{
					character.SetVelocityX(0);
				}
				else if (character.Velocity.x  < 0 && character.Velocity.x > -quiesceSpeed)
				{
					character.SetVelocityX(0);
				}
			}

			// Translate
			character.Translate(character.Velocity.x * TimeManager.FrameTime, 0, false);

			hasAppliedForce = false;
		}

		override protected void MoveInY ()
		{
			// Apply gravity
			if (!character.Grounded || character.Velocity.y > 0)
			{
				character.AddVelocity(0, TimeManager.FrameTime * character.DefaultGravity, false);
			}
			// Translate
			character.Translate(0, character.Velocity.y * TimeManager.FrameTime, true);
		}

		override public void DoJump()
		{
			jumpStart = true;
			jumpCount++;

			// If we are not grounded this MUST be a double jump
			if (character.TimeSinceGroundedOrOnLadder > jumpGroundedLeeway) jumpCount = 2;

			if (jumpCount == 2)
			{

				Vector2 velocity = new Vector2(character.Velocity.x, doubleJumpVelocity);
				character.SetVelocityX(velocity.x);
				character.SetVelocityY(velocity.y);

                EventDispatcher.Publish < CharacterEvent > ( this, CharacterEvent.OnDoubleJump, CharacterBridge.playerId,  null );
			}
			else 
			{

				Vector2 velocity = new Vector2(character.Velocity.x, jumpVelocity);
				//				velocity = Quaternion.Euler(0, 0, character.transform.rotation.eulerAngles.z) * velocity;
				character.SetVelocityX(velocity.x);
				character.SetVelocityY(velocity.y);

                EventDispatcher.Publish < CharacterEvent > ( this, CharacterEvent.OnJump, CharacterBridge.playerId, null );
			}

			if (character.Velocity.x > 0) initialDirection = 1;
			else if (character.Velocity.x < 0) initialDirection = -1;
			else initialDirection = 0;
			hasRecievedCounterInput = false;
		}
		override public void DoOverridenJump(float newHeight, int jumpCount)
		{
			jumpStart = true;
			this.jumpCount = jumpCount;
			float initialVelocity = Mathf.Sqrt(-2.0f * character.DefaultGravity  * newHeight);
			Vector2 velocity = new Vector2(character.Velocity.x, initialVelocity);
			character.SetVelocityX(velocity.x);
			character.SetVelocityY(velocity.y);

			if (character.Velocity.x > 0) initialDirection = 1;
			else if (character.Velocity.x < 0) initialDirection = -1;
			else initialDirection = 0;
			hasRecievedCounterInput = false;
		}

		virtual protected void DoRotation()
		{
			// Determine the point we will rotate around
			float difference  = 0 - character.transform.eulerAngles.z;
			// Shouldn't really happen but just in case
			if (difference > 180) difference = difference - 360;
			if (difference < -180) difference = difference + 360;

			Vector3 rotateAround = character.transform.position;

			if (difference >  character.rotationSpeed * TimeManager.FrameTime) difference =  character.rotationSpeed * TimeManager.FrameTime;
			if (difference < - character.rotationSpeed * TimeManager.FrameTime) difference = - character.rotationSpeed * TimeManager.FrameTime;
			character.transform.RotateAround(rotateAround, new Vector3(0,0,1), difference);
		}

	}
}
