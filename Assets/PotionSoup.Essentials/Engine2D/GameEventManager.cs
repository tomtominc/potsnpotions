﻿using UnityEngine;
using System.Collections;
using PotionSoup.Dispatcher;
using System;

namespace PotionSoup.Engine2D
{
    public class GameEventManager : Singleton < GameEventManager >
    {
        public static void PauseGame ()
        {
            TimeManager.Instance.Pause(true);
            EventDispatcher.Publish < GameEvent > (Instance, GameEvent.OnPaused, null, null);
        }

        public static void ResumeGame ()
        {
            TimeManager.Instance.UnPause(true);
            EventDispatcher.Publish < GameEvent > (Instance, GameEvent.OnUnPaused, null, null);
        }

        public static void SlowMo ( float time )
        {
            
        }

        public static void SlowMoWithVinette ( float time, Vector3[] positions, float[] sizes )
        {
            
        }

        public static void TimeStop ( float time )
        {
            Time.timeScale = 0;

            Timer timer = new Timer ( time, () => Time.timeScale = 1f );

            timer.Start ( Instance );
        }

        public static void StopAllDamage ( float time = -1 )
        {
            // characters cannot be damaged
        }

        public static void StartAllDamage ( float time = -1 )
        {
            // characters can now be damaged
        }



    }
}

