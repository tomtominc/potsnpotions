﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.Dispatcher
{
	public class EventDispatcher 
	{
		public static void Subscribe < T > (T eMessageType, object rFilter, MessageHandler rHandler, bool rImmediate = true)
		{
			MessageDispatcher.AddListener (eMessageType.ToString(), rFilter == null ? "" : rFilter.ToString() , rHandler, rImmediate);
		}

		public static void UnSubscribe < T >  (T eMessageType, object rFilter, MessageHandler rHandler, bool rImmediate = true)
		{
            MessageDispatcher.RemoveListener (eMessageType.ToString(), rFilter == null ? "" : rFilter.ToString() ,rHandler,rImmediate);
		}
              
        public static void Publish < T > ( object rSender, T eMessageType, object rFilter, object rData)
		{
            MessageDispatcher.SendMessage ( rSender, rFilter == null ? "" : rFilter.ToString(), eMessageType.ToString(), rData, 0f);
		}
	}
}
