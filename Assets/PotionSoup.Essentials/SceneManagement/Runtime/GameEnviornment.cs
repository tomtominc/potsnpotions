﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using PotionSoup.Dispatcher;

namespace PotionSoup.SceneManagement
{
	public class GameEnviornment : Singleton<GameEnviornment> 
	{

		private static List<Scene> AllLoadedScenes = new List<Scene>();

		public static Scene lastScene;
		public static Scene currentScene;

		public static bool HasSceneBeenLoaded (Scene scene)
		{
			return AllLoadedScenes.Contains(scene); 
		}

		public static bool HasSceneBeenLoaded (string scene)
		{
			return AllLoadedScenes.Contains(GetScene(scene));
		}

		public static Scene GetScene (string scene)
		{
			return SceneManager.GetSceneByName(scene);
		}

		private void OnLevelWasLoaded(int level) 
		{
			AllLoadedScenes.Add(SceneManager.GetSceneAt(level));
		}

		public static void LoadScene ( string scene )
		{
            Hashtable datatable = new Hashtable ()
            {
                { "ActiveScene" , SceneManager.GetActiveScene() },
                { "NextScene" , GetScene(scene) }
            };

            EventDispatcher.Publish < SceneEvent > ( Instance, SceneEvent.OnExitScene, null, datatable);

			SceneManager.LoadScene(scene );
		}
	}
}
