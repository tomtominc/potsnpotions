﻿using UnityEngine;
using System.Collections;
using System.IO;

namespace PotionSoup.Persistent
{
    public class EditorPersistentSettings
    {
        protected static Perlib instance;
        protected static Perlib Instance
        {
            get 
            {
                if (instance == null)
                {
                 	// should always be at this path
					// crucial for correct storage settings

					FileInfo info = new FileInfo(Application.dataPath 
						+ "/PotionSoup.Essentials/PotionSoup.Persistent" +
							"/EditorPersistent/EditorSaveData.sav");
					
                    instance = new Perlib(info);
                    instance.Open();
                }

                return instance;
            }
        }

        public static void ResetSettings ()
        {
            instance = null;
        }

        public static T GetValue<T> (string key, T defaultValue)
        {
            string value = Instance.GetValue<string>(key,string.Empty);
            if (value == string.Empty) return defaultValue;
            else return DeserializeObject<T>(value);
        }

        public static string GetValueRaw (string key, string defaultValue)
        {
            string value = Instance.GetValue<string>(key,string.Empty);

            if (value == string.Empty)
            { 
                return defaultValue;
            }

            return value;
        }

        public static void SetValue(string key, object value)
        {
            string serialized = SerializeObject(value);
            Instance.SetValue(key,serialized);
            Instance.Save();
        }

        private static T DeserializeObject<T> (string serialized)
        {
			return JsonUtility.FromJson<T>(serialized);
        }

        private static string SerializeObject(object value)
        {
			return JsonUtility.ToJson(value,false);
        }
    }
}
