﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace PotionSoup.Persistent
{
    public class PersistentSettings
    {
        protected static Perlib instance;
        
		protected static Perlib Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new Perlib("GameSettings.sav","2avAux4HUdUEB$N5(hH;Qb%.>");
                    instance.Open();
                }

                return instance;
            }
        }

        public static T GetValue<T> (string key, T defaultValue)
        {
            if (!Instance.HasKey(key))
			{
				SetValue(key,defaultValue);
			}

            string value = Instance.GetValue<string>(key,string.Empty);

            if (value == string.Empty) 
				return defaultValue;
            else 
				return DeserializeObject<T>(value);
        }

        public static void SetValue(string key, object value)
        {
            string serialized = SerializeObject(value);
            Instance.SetValue(key,serialized);
            Instance.Save();
        }

        public static T DeserializeObject<T> (string serialized)
        {
			return JsonUtility.FromJson<T>(serialized);
        }

        public static string SerializeObject(object value)
        {
			return JsonUtility.ToJson(value, false);
        }
    }
}
