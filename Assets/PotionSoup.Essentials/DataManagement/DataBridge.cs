﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Persistable;

public class DataBridge : MonoBehaviour 
{
    private static DataBridge instance;
	private static DataBridge Instance
	{
		get 
		{
			if ( instance == null )
			{
				instance = GameObject.FindObjectOfType<DataBridge>();
			}

			return instance;
		}
	}
	private Perlib dataStorage;
	private static Perlib Storage
	{
		get 
		{
			if ( Instance.dataStorage == null )
			{
				Instance.dataStorage = new Perlib ( "datastorage.sav", "random_data_stuff" );
                Instance.dataStorage.Open();
			}

			return instance.dataStorage;
		}
	}

	public static T GetValue<T> ( string key, T defaultValue )
	{
		return Storage.GetValue<T>(key);
	}

	public static void SetValue<T> (string key, T value )
	{
		Storage.SetValue<T>(key, value);
	}

	public void Start () {}

}

