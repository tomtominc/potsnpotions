﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using PotionSoup.Dispatcher;

public enum PlayerID 
{
	one = 0, two = 1, three = 2, four = 3
}
namespace PotionSoup.Input
{
	public enum InputState
	{
		Disabled = -1, Menu = 4, Character = 5
	}

	public enum InputType
	{
		Invalid, Keyboard, Xbox360, XboxOne, Ps3, Ps4, WiiMote, WiiGamePad, Unidentified
	}

	public class InputBridge : Singleton < InputBridge > 
	{

		public PlayerID systemPlayerID = PlayerID.one;
        public InputState initialSytemPlayerInputState;
        public InputState initalPlayerInputStates;

		protected bool isReady = false;

		protected static InputState systemState;
		protected Player _systemPlayer;
		private static Dictionary<PlayerID, Player> Players = new Dictionary<PlayerID, Player>();

        private static Dictionary < string , InputType > InputTypeMapping = new Dictionary<string, InputType>()
        {
            { "Keyboard", InputType.Keyboard },
            { "Xbox 360 Controller", InputType.Xbox360 }
        };

		private void Awake ()
		{
			ReInput.ControllerConnectedEvent += OnControllerConnectedEvent;
			ReInput.ControllerDisconnectedEvent += OnControllerDisconnectedEvent;
			ReInput.ControllerPreDisconnectEvent += OnControllerPreDisconnectEvent;

            SetPlayers();
            SetSystemPlayer(systemPlayerID);
            SetSystemPlayerState(initialSytemPlayerInputState);
            SetPlayerStates(initalPlayerInputStates);

		}

		private IEnumerator Start ()
		{
			yield return new WaitForSeconds (1f);

			// set everyone again, this allows controllers to be assigned.
			SetSystemPlayer(systemPlayerID);
			SetSystemPlayerState(initialSytemPlayerInputState);
			SetPlayerStates(initalPlayerInputStates);
			ValidateAllPlayers();

			isReady = true;
		}


		private void OnControllerConnectedEvent ( ControllerStatusChangedEventArgs args )
		{
			
		}

		private void OnControllerDisconnectedEvent ( ControllerStatusChangedEventArgs args )
		{

		}

		private void OnControllerPreDisconnectEvent ( ControllerStatusChangedEventArgs args )
		{

		}

		private static void SetPlayers ()
		{
			foreach (var player in ReInput.players.Players)
			{
				Players.Add ((PlayerID)player.id,player);
			}
		}

		public static Player GetPlayer (PlayerID playerId)
		{
			return Players[playerId];
		}

		public static Player GetSystemPlayer ()
		{
			return Instance._systemPlayer;
		}

		public static int GetControllerCount ()
		{
			int joysticks = ReInput.controllers.GetControllerCount ( ControllerType.Joystick );
			int keyboards = ReInput.controllers.GetControllerCount ( ControllerType.Keyboard );

			return joysticks + keyboards;
		}

		public static int GetJoystickCount ()
		{
			return ReInput.controllers.GetControllerCount ( ControllerType.Joystick );
		}

		public static InputType GetControllerType (PlayerID playerId)
		{
			Player player = GetPlayer  (playerId);
            var maps = player.controllers.maps.GetAllMaps();

            InputType type = InputType.Invalid;

            foreach ( var map in maps )
            {
                if (map.enabled)
                {
                    var controller = ReInput.controllers.GetController ( map.controllerType, map.controllerId );
                    if (controller != null )
                    {
                        if (!InputTypeMapping.ContainsKey( controller.name ))
                        {
                            Debug.LogWarning("'"+controller.name + "'" + " is not mapped to the InputTyping.");
                            Debug.LogWarning("Input type mapping count: " + InputTypeMapping.Count); 
                            foreach ( var pair in InputTypeMapping) Debug.LogWarning("Key: '" + pair.Key + "' Value: " + pair.Value ); 
                            return InputType.Invalid;
                        }

                        type = InputTypeMapping [ controller.name ];
                    }
                }
            }

            return type;
		}

        public static string GetControllerName ( PlayerID playerId )
        {

            Player player = GetPlayer  (playerId);
            var maps = player.controllers.maps.GetAllMaps();

            foreach ( var map in maps )
            {
                if (map.enabled)
                {
                    var controller = ReInput.controllers.GetController ( map.controllerType, map.controllerId );
                    if (controller != null )
                    {
                        return controller.name;
                    }
                }
            }

            return "Invalid";
        }

		public static void SetSystemPlayer ( PlayerID playerId )
		{
			Instance._systemPlayer = Players[playerId];
		}

		public static void SetSystemPlayerState ( InputState state )
		{
			InputBridge.DisableMaps(Instance.systemPlayerID);
			InputBridge.EnableMaps (Instance.systemPlayerID,state);

			Hashtable datatable = new Hashtable ()
			{
				{ "SystemPlayer", GetSystemPlayer() },
				{ "State" , state }
			};

			EventDispatcher.Publish < InputEvent > ( Instance, InputEvent.OnChangeSystemPlayer , datatable, 0 );
		}
			
		private static void DisableMaps (PlayerID playerId)
		{
			Player player = GetPlayer(playerId);
			player.controllers.maps.SetAllMapsEnabled( false );
		
		}
		private static void EnableMaps (PlayerID playerId,InputState state)
		{
			Player player = GetPlayer(playerId);
			player.controllers.maps.SetMapsEnabled ( true , (int)state );
		}

		public static void SetPlayerStates ( InputState state )
		{
			//sets all player states except for the system player
			foreach (var pair in Players)
			{
				if (pair.Key != Instance.systemPlayerID)
				{
					SetPlayerState(pair.Key,state);
				}
			}
		}

		public static void PrintAllMapInfo ()
		{
			foreach (var pair in Players)
			{
				PrintMapInfo (pair.Key);
			}
		}
		public static void PrintMapInfo (PlayerID playerId)
		{
			Player player = GetPlayer  (playerId);
			var maps = player.controllers.maps.GetAllMaps();
			ControllerType type = ControllerType.Custom;
			foreach ( var map in maps )
			{
				type = map.controllerType;

				Debug.Log (player.name + " Map controller type: " +  map.controllerType);
				Debug.Log (player.name + " Map Identifier: " + map.categoryId);
				Debug.Log (player.name + " Map conroller name: " + ReInput.controllers.GetController(type,map.controllerId).id);
				Debug.Log (player.name + " Map enabled: " + map.enabled);
				Debug.Log ("----------------------------------------------------------------");
			}
		}

		public static bool IsReady ()
		{
			return Instance.isReady;
		}

		public static void SetAllPlayerStates ( InputState state )
		{
			foreach (var pair in Players)
			{
				SetPlayerState(pair.Key,state);
			}
		}

		public static void SetPlayerState (PlayerID playerId, InputState state)
		{
			InputBridge.DisableMaps(playerId);
			InputBridge.EnableMaps (playerId,state);
		}

		public static void ValidateAllPlayers ()
		{
			foreach ( var pair in Players )
			{
				ValidatePlayer (pair.Key);
			}
		}

		public static void ValidatePlayer (PlayerID playerId)
		{
			Player player = GetPlayer (playerId);
			var maps = player.controllers.maps.GetAllMaps();
			bool isJoystickConnected = false;
			foreach ( var map in maps )
			{
				if (map.controllerType == ControllerType.Joystick)
				{
					isJoystickConnected = true;
				}
				else if (map.controllerType == ControllerType.Keyboard)
				{
					if (isJoystickConnected == true)
					{
						map.enabled = false;
					}
				}
			}

			foreach ( var map in maps )
			{
				if (map.controllerType == ControllerType.Keyboard)
				{
					if (isJoystickConnected == true)
					{
						map.enabled = false;
					}
				}
			}


		}

		public static InputState GetPlayerState ( PlayerID playerId )
		{
			Player player = ReInput.players.GetPlayer((int)playerId);
			InputState state = InputState.Disabled;

			foreach ( var map in player.controllers.maps.GetAllMaps())
			{
				if (map.enabled == true)
				{
					state = (InputState)map.categoryId;
				}
			}

			return state;
		}

		private void AssignControllers ()
		{
            foreach (Joystick j in ReInput.controllers.Joysticks)
            {
                if (ReInput.controllers.IsJoystickAssigned(j)) continue; // Joystick is already assigned

                // Assign Joystick to first Player that doesn't have any assigned
                AssignJoystickToNextOpenPlayer(j);
            }
		}

        void AssignJoystickToNextOpenPlayer(Joystick j)
        {
            foreach (Player p in ReInput.players.Players)
            {
                //if (p.controllers.joystickCount > 0) continue; // player already has a joystick

                Debug.Log(p.name + " was assigned " + j.name);
                p.controllers.AddController(j, true); // assign joystick to player
                return;
            }
        }
	}
}