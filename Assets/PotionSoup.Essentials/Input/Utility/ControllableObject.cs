﻿using UnityEngine;
using System.Collections;
using Rewired;

namespace PotionSoup.Input
{
	public class ControllableObject : MonoBehaviour 
	{
		public PlayerID playerId;
		protected Player _player;

        protected InputType _inputType = InputType.Invalid;
        protected bool _inputActive = true;

        protected bool isEnabled = true;

		private IEnumerator Start ()
		{
			while (!InputBridge.IsReady())
				yield return null;

            _player = InputBridge.GetPlayer (playerId);
            _inputType = InputBridge.GetControllerType(playerId);
			Initialize();
		}

		private void Update ()
		{
            if (_player == null) return;
            if (_inputType == InputType.Invalid) return;
            if (isEnabled == false) return;

			HandleInput ();
		}

		protected virtual void Initialize ()
		{
			
		}

		protected virtual void HandleInput ()
		{
			
		}
	}
}
