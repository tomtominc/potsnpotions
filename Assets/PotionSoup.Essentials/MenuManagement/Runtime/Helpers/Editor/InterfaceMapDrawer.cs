﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace PotionSoup.MenuManagement
{
    [CustomPropertyDrawer(typeof(InterfaceMap))]
    public class InterfaceMapDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 6f;
        }

        public void DoPopup < T > (SerializedProperty property, Rect position, SerializedProperty memberProperty, int executionValue )
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

            if ( property.objectReferenceValue == null )
            {
                return;
            }


            List< string > fields = new List<string> ();

            if ( executionValue == 0 )
                fields = property.objectReferenceValue.GetType().GetProperties(flags).ToList().ConvertAll(x => string.Format("{0} ({1})",x.Name ,x.PropertyType ));
            else if ( executionValue == 1 )
                fields = property.objectReferenceValue.GetType().GetMethods(flags).ToList().ConvertAll(x => string.Format("{0}", x.Name ));

            fields.Sort();

            int currentIndex = Array.FindIndex < string > (fields.ToArray(), x => x.Contains( memberProperty.stringValue ));

            currentIndex = EditorGUI.Popup(new Rect (position.x,position.y,position.width,16f), "Member Field", currentIndex, fields.ToArray() );

            if ( currentIndex < fields.Count && currentIndex > -1 )
            {
                memberProperty.stringValue = fields[currentIndex].Split(' ')[0];
            }

        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var enabledProperty = property.FindPropertyRelative("enabled");
            var idProperty = property.FindPropertyRelative("propertyId");
            var typeProperty = property.FindPropertyRelative("type");
            var imageProperty = property.FindPropertyRelative("image");
            var textProperty = property.FindPropertyRelative("text");
            var buttonProperty = property.FindPropertyRelative("button");
            var animatorProperty = property.FindPropertyRelative("animator");
            var memberProperty = property.FindPropertyRelative("member");
            var executionTypeProperty = property.FindPropertyRelative("executionType");

            EditorGUI.PropertyField(position, enabledProperty, true);

            position.y += 16f;

            EditorGUI.PropertyField(position,idProperty, true);

            position.y += 16f;

            EditorGUI.PropertyField(position,typeProperty, true);
            int typeValue = typeProperty.enumValueIndex;

			position.y += 16f;

            EditorGUI.PropertyField(position,executionTypeProperty,true);
            int executionValue = executionTypeProperty.enumValueIndex;

            position.y += 16f;
            
			if (typeValue == 0)
            {
                DoPopup < Text > ( textProperty, position, memberProperty, executionValue );

                position.y += 16f;

                EditorGUI.PropertyField(position,textProperty, true);

            }
			else if (typeValue == 1)
            {
                DoPopup < Image > ( imageProperty, position, memberProperty,executionValue );

                position.y += 16f;

                EditorGUI.PropertyField(position,imageProperty, true); 
            }
            else if ( typeValue == 2 )
            {
                DoPopup < Button > ( buttonProperty, position, memberProperty,executionValue );

                position.y += 16f;


                EditorGUI.PropertyField( position,buttonProperty,true);
            }
            else if ( typeValue == 3 )
            {
                DoPopup<Animator>( animatorProperty, position, memberProperty,executionValue );

                position.y += 16f;

                EditorGUI.PropertyField(position, animatorProperty, true);
            }


            EditorGUI.EndProperty();
        }
    }
}
