﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Input;
using PotionSoup.MenuManagement.GameLogic;
using UnityEngine.Events;
using PotionSoup.Dispatcher;

namespace PotionSoup.MenuManagement
{
    public enum SelectionState
    {
		None,
        NotReady,
        Ready
    }

    public class CharacterSelection : ControllableObject
    {
        public SelectionState state;
        public InterfaceDatabase characterDatabase;
        public float joystickSensitivity = 0.1f;
        public Image controllerType;
        public List < GameObject > DisabledItems = new List< GameObject > ();

        public float disabledTimeOnSelected = 2f;

        protected InterfaceBinding _binding;

        protected int _currentIndex = 0;
        protected InterfaceObject _currentSelection;

		protected override void Initialize ()
		{
            _currentIndex = (int)playerId;
            _binding = GetComponent < InterfaceBinding > ();

            if (controllerType != null )
                controllerType.sprite = (Sprite)MenuManager.GetAsset(new AssetKey ( AssetCategory.Input, _inputType ));
            
            GoToStateNotReady();
        }

		protected override void HandleInput ()
        {
            float horz = _player.GetAxis(StringManager.input_menu_horizontal);
            bool positive = _player.GetButtonDown(StringManager.input_menu_positive);
            bool negative = _player.GetButtonDown(StringManager.input_menu_negative);


            if ( state == SelectionState.NotReady )
            {
                if (_inputActive == false && Mathf.Abs(horz) < joystickSensitivity && !positive)
                {
                    _inputActive = true;
                }

                if (_inputActive == false ) return;

                if ( horz > joystickSensitivity )
                {   
                    _inputActive = false;
                    SetToNextCharacter();
                }
                if ( horz < -joystickSensitivity )
                {
                    _inputActive = false;
                    SetToPreviousCharacter();
                }

                if ( positive && _inputActive )
                {
                    GoToStateReady();
                }
            }
            else if ( state == SelectionState.Ready )
            {

                if ( negative && _inputActive )
                {
                    GoToStateNotReady();
                }
            }

		}

        public virtual void GoToStateNone ()
        {
            state = SelectionState.None;

            foreach ( var obj in DisabledItems )
            {
                obj.SetActive(false);
            }
            SetCharacterMenuInterface(null);
        }

        public virtual void GoToStateNotReady()
        {
            if ( _inputType == InputType.Invalid ) 
            {
                GoToStateNone();
                return;
            }

            state = SelectionState.NotReady;

            if (_currentSelection == null )
            {
                return;
            }

            characterDatabase.DisableObject(_currentIndex);
            _currentSelection = characterDatabase.GetObjectbyIndex(_currentIndex);

            SetCharacter(_currentSelection);

			Hashtable data = new Hashtable ()
			{
				{"PlayerId", playerId}
			};

			EventDispatcher.Publish < CharacterSelectEvent > (this, CharacterSelectEvent.OnDeselect , data, 0 );
        }

        public virtual void GoToStateReady()
        {
            state = SelectionState.Ready;

            characterDatabase.EnableObject(_currentIndex);
            _currentSelection = characterDatabase.GetObjectbyIndex(_currentIndex);

            SetCharacter(_currentSelection);

			Hashtable data = new Hashtable ()
			{
				{"Character", _currentSelection},
				{"PlayerId", playerId}
			};

            isEnabled = false;
            StartCoroutine ( SetEnabled(disabledTimeOnSelected, true) );

			EventDispatcher.Publish < CharacterSelectEvent > (this, CharacterSelectEvent.OnSelect, data, 0 );
        }

        public IEnumerator SetEnabled ( float time, bool enable )
        {
            yield return new WaitForSeconds(time);
            isEnabled = enable;
        }

        public virtual void SetCharacter(InterfaceObject selection)
        {
            if ( _inputType == InputType.Invalid )
            {
                return;
            }

            InterfaceObject_Character characterData = selection as InterfaceObject_Character;

            if (characterData == null )
            {   
                Debug.LogWarning("Trying to set a character with null data: " + name); 
                return;
            }

            SetCharacterMenuInterface(characterData);
         
        }

        public virtual void SetCharacterMenuInterface ( InterfaceObject_Character characterData )
        {
            _binding.BindData( characterData );
        }

        public virtual void SetToNextCharacter()
        {
            if (characterDatabase == null)
                return;

            _currentIndex = _currentIndex.GetWrapped(0, characterDatabase.Count()-1, 1);
            _currentSelection = (InterfaceObject)characterDatabase.GetObjectbyIndex(_currentIndex);

            EventDispatcher.Publish < CharacterSelectEvent > ( this, CharacterSelectEvent.OnHorizontalRight , playerId, null );

            SetCharacter(_currentSelection);
        }

        public virtual void SetToPreviousCharacter()
        {
            if (characterDatabase == null)
                return;

            _currentIndex = _currentIndex.GetWrapped(0, characterDatabase.Count()-1, -1);
            _currentSelection = (InterfaceObject)characterDatabase.GetObjectbyIndex(_currentIndex);

            EventDispatcher.Publish < CharacterSelectEvent > ( this, CharacterSelectEvent.OnHorizontalLeft , playerId , null);

            SetCharacter(_currentSelection);
        }

    }
}
