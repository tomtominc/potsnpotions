﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PotionSoup.MenuManagement
{
	public class InterfaceObject : ScriptableObject 
    {
        public bool enabled = true;

		[Tooltip("The a nice name to display. Sometimes just 'name' can be used.")]
		public string displayName = "Interface Object";

		[Tooltip("The display graphic of the object, used for various things.")]
       	public Sprite displayGraphic;

		[Tooltip("The description of the current object.")]
        [TextArea] public string description;

        public virtual void Toggle ()
        {
            enabled = !enabled;
        }

		public virtual void DoAction ()
		{
			// override to give the interface object an action.
		}

        public static InterfaceObject New ()
        {
            return CreateInstance<InterfaceObject> ();
        }

		public static InterfaceObject New (string displayName, Sprite displayGraphic, string description)
        {
            var instance = CreateInstance<InterfaceObject>();

            instance.displayName = displayName;
			instance.displayGraphic = displayGraphic;
			instance.description = description;

            return instance;
        }

	}
}