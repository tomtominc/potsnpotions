﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Reflection;

namespace PotionSoup.MenuManagement
{
    [System.Serializable]
    public class InterfaceMap
    {
        public enum Type
        {
            Text,
            Image,
            Button,
            Animator
        }

        public enum ExecutionType
        {
            Property,
            Method
        }

        public bool enabled = true;
        public Type type;
        public string propertyId;
        public ExecutionType executionType;
        public string member;

        public Image image;
        public Text text;
        public Button button;
        public Animator animator;

        public void SetValue(object value)
        {
            if ( enabled == false )
            {
                return;
            }
            Debug.Log(enabled); 
            switch (type)
            {
             
                case Type.Image:
                    image.GetType().GetProperty(member).SetValue(image, value, null);
                    break;
                case Type.Text:
                    text.GetType().GetProperty(member).SetValue(text, value, null);
                    break;
                case Type.Button:
                    button.GetType().GetProperty(member).SetValue(button, value, null);
                    break;
                case Type.Animator:
                    if (executionType == ExecutionType.Property)
                    {
                        animator.GetType().GetProperty(member).SetValue(animator, value, null);
                    }
                    else if (executionType == ExecutionType.Method)
                    {
                        const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

                        MethodInfo method = animator.GetType().GetMethod(member, new[]{typeof(string)});

                        Debug.Log(method.Name);

                        if (method == null)
                        {
                            Debug.Log("null method");
                            return;
                        }

                        //method.Invoke(animator, new object[] { value });
                    }
                    break;
            }
        }

    }
}