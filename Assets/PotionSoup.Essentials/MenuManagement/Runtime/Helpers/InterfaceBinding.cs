﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace PotionSoup.MenuManagement
{
    public class InterfaceBinding : MonoBehaviour 
    {
        public List < InterfaceMap > mapping = new List < InterfaceMap > ();

        public object objectData { get; set; }

        protected Dictionary < string , object > datatable { get; set; }

        public virtual void BindData ( object data )
        {
            if ( data == null ) return;

            objectData = data;
            datatable = objectData.AsDictionary();

            foreach (var map in mapping )
            {
                string lowerName = map.propertyId.ToLower();

                if (datatable.ContainsKey(lowerName))
                {
                    map.SetValue(datatable[lowerName]);
                }
            }
        }
    }
}
