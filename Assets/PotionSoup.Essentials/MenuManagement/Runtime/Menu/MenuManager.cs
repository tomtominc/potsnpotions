﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Dispatcher;
using DG.Tweening;
using PotionSoup.Engine2D;
using EasyEditor;
using System.Linq;

namespace PotionSoup.MenuManagement
{
    [RequireComponent(typeof(NavigationManagement))]
    public class MenuManager : Singleton < MenuManager >
    {
        [Inspector ( group =  "General Settings" )]
        public MenuController initialView;
        public InterfaceAssetDatabase assetDatabase;

        protected NavigationManagement _navigation;
        protected MenuController _currentMenu;
        protected MenuController _previousMenu;

        protected Dictionary < string , MenuController > Menus = new Dictionary<string, MenuController> ();

        private void Start()
        {
            assetDatabase.Initialize();

            _navigation = GetComponent < NavigationManagement >();

            _navigation.Initialize();

            var menus = GetComponentsInChildren < MenuController > ( true );

            foreach ( var menu in menus )
            {
                if ( Menus.ContainsKey( menu.name ) )
                {
                    Debug.LogError("Menu with the same name exists, please change name of " + menu.name );
                    return;
                }

                Menus.Add(menu.name, menu); 
            }

            if (initialView != null)
            {
                OpenMenu(null, initialView, null);
            }
        }

      
        public void OpenPopup ( string popup )
        {
            InterfaceBinding interfaceBinding = _navigation.GetCurrentInterfaceBinding();
            if (interfaceBinding == null)
            {
                OpenPopup( popup, null );
                return;
            }

            OpenPopup(popup , interfaceBinding.objectData  );
        }

        public void OpenPopup ( string popup , object data = null )
        {
            if ( ! Menus.ContainsKey(popup) ) return;

            MenuController controller = Menus[ popup ];

            if ( controller == null ) return;

            OpenPopup(controller, data);
          
        }

        public void OpenPopup ( MenuController controller )
        {
            InterfaceBinding interfaceBinding = _navigation.GetCurrentInterfaceBinding();

            if (interfaceBinding == null)
            {
                OpenPopup( controller , null );
                return;
            }

            OpenPopup(controller, interfaceBinding.objectData);
        }

        public void OpenPopup ( MenuController controller , object data = null )
        {
            if ( controller == null ) return;

            OpenMenu(null, controller, null, data ); 
        }


        public void OpenMenu ( Transition transition , object data = null )
        {
            if (transition == null || transition.ExitBehaviour == ExitBehaviour.Disabled)
                return;
            
            if (transition.ExitBehaviour == ExitBehaviour.ExitOnly)
            {
                CloseMenu(_currentMenu);
            }
            else
            {
                OpenMenu(transition, transition.nextMenu, _currentMenu, data);
            }
        }

        public void OpenMenu(Transition transition, MenuController enterMenu, MenuController exitMenu, object data = null)
        {
            if (enterMenu == null) return;

            DisableNavigation ( false );

            enterMenu.Initialize ( data );

            float exitDuration = transition == null ? (exitMenu == null) ? 0f : exitMenu.Exit() : 
                (exitMenu == null || transition.ExitBehaviour == ExitBehaviour.OpenNextMenu) ? 0f : exitMenu.Exit();
            
            float enterDelay = transition == null ? 0f : transition.GetEnterDelay(exitDuration);

            _previousMenu = _currentMenu;
            _currentMenu = enterMenu;

            Timer enterTransitionTimer = new Timer(enterDelay, () =>
                {
                    float enterDuration = enterMenu.Enter();

                    if (enterMenu.NavigationOption == NavigationOption.Disabled)
                        return;

                    float navigationEnableDuration = enterMenu.GetNavigationEnableDelay(enterDuration);
                    Timer enableNavigationTimer = new Timer(navigationEnableDuration, () => EnableNavigation());

                    enableNavigationTimer.Start(this); 

                });

            enterTransitionTimer.Start(this); 

        }

        public void OpenPreviousMenu ()
        {
            if ( _currentMenu == null ) return;

            MenuController backMenu = _currentMenu.backMenu;

            if ( backMenu == null ) return;

            Transition transition = _currentMenu.TryFindTransition(backMenu);

            if ( transition == null )
            {
                CloseMenu(_currentMenu);
            }
            else
            {
                OpenMenu(transition, backMenu, _currentMenu, null );
            }

        }

        public void CloseMenu ( MenuController menu )
        {
            if (menu == null)
                return;

            DisableNavigation(false);

            float navigationEnabledDelay = _previousMenu.GetNavigationEnableDelay(_currentMenu.Exit()); 

            _currentMenu = _previousMenu;

            _previousMenu = null;

            if (_currentMenu.NavigationOption == NavigationOption.Disabled)
                return;

            Timer timer = new Timer(navigationEnabledDelay, () => EnableNavigation());

            timer.Start(this); 

        }

        public void DoActionOnCurrentInterfaceObject()
        {
            InterfaceBinding interfaceBinding = _navigation.GetCurrentInterfaceBinding();
            if (interfaceBinding == null)
                return;

            InterfaceObject interfaceObject = interfaceBinding.objectData as InterfaceObject;
            if (interfaceObject == null)
                return;

            interfaceObject.DoAction();

            interfaceBinding.BindData(interfaceObject);
        }

        public void DisableNavigation(bool saveLast)
        {
            if (_currentMenu != null)
                _currentMenu.SetInteractability(false);
            if (_navigation != null)
                _navigation.DisableNavigation(saveLast);
        }

        public void EnableNavigation()
        {
            if (_currentMenu != null)
                _currentMenu.SetInteractability(true);
            if (_navigation != null)
                _navigation.EnableNavigation(_currentMenu);
        }

        public static Object GetAsset(AssetKey key)
        {
            return Instance.assetDatabase.GetAsset(key);
        }
    }
}
