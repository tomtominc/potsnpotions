using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using EasyEditor;
using System.Linq;

namespace PotionSoup.MenuManagement
{
    [Groups( "Navigation Settings", "Transition Settings" , "Transition Modules" )]
	[CustomEditor (typeof (MenuController), true)]
    public class MenuControllerEditor : EasyEditorBase 
	{
		private MenuController menu;
        private bool foldout = false;

        [Inspector( group = "Transition Settings", rendererType = "CustomRenderer")]
        private void InspectorGUI () 
		{
            menu = target as MenuController;

            Button[] Buttons = menu.GetComponentsInChildren<Button>();

            if ( Buttons.Length < 1 )
            {
                EditorGUILayout.HelpBox("Please Add Buttons Under the Menu to get Transitions!", UnityEditor.MessageType.Info);
                return;
            }

            string[] buttonNames = Buttons.ToList().ConvertAll( x => x.name ).ToArray();

            for ( int i = 0; i < Buttons.Length; i++ )
            {
                Transition transition = Buttons[i].GetComponent < Transition > ();

                if ( transition == null )
                {
                    transition = Buttons[i].gameObject.AddComponent < Transition > ();
                }
            }

            int selection = EditorGUIExtensions.DrawSelectableHeader ( "Menu Controller Transitions" + menu.GetUniqueGuid(), GUIStyle.none, 
                EditorColor.green, buttonNames );

            EditorGUIExtensions.BeginContent(Color.white);
            {
                if ( selection >= Buttons.Length ) return;

                Button button = Buttons[selection];
                if ( button == null ) return;

                Transition transition = button.GetComponent < Transition > ();
                if ( transition == null )
                {
                    transition = button.gameObject.AddComponent < Transition > ();
                }

                transition.ExitBehaviour = (ExitBehaviour)EditorGUILayout.EnumPopup("Exit Behaviour", transition.ExitBehaviour);

                if ( transition.ExitBehaviour != ExitBehaviour.ExitOnly && transition.ExitBehaviour != ExitBehaviour.Disabled )
                {
                    transition.NextMenuEnterBehaviour = (NextMenuEnterBehaviour)EditorGUILayout.EnumPopup("Next Menu Enter Behaviour", transition.NextMenuEnterBehaviour);
                    transition.nextMenu = (MenuController)EditorGUILayout.ObjectField("Next Menu", transition.nextMenu, typeof(MenuController), true );

                    if ( transition.NextMenuEnterBehaviour == NextMenuEnterBehaviour.FixedTime )
                    {
                        transition.nextMenuDelay = EditorGUILayout.FloatField("Next Menu Delay", transition.nextMenuDelay);
                    }
                }
            } 
            EditorGUIExtensions.EndContent ();
		}
	}
}
