﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using EasyEditor;
using PotionSoup.Engine2D;

namespace PotionSoup.MenuManagement
{
    public enum NavigationOption
    {
        Disabled, Initialization, Focused, FixedTime
    }

   
    [RequireComponent(typeof(CanvasGroup))]
	public class MenuController : MonoBehaviour 
	{
        [Inspector ( group = "Navigation Settings" )]
        public NavigationOption NavigationOption;

        [Visibility ("NavigationOption", 
                NavigationOption.FixedTime, 
                NavigationOption.Initialization,
                NavigationOption.Focused ) ]
		public Selectable initiallyFocusedSelectable;
		
        [Visibility( "NavigationOption",NavigationOption.FixedTime )]
        public float defaultNavigationEnableDelay = 0.1f;

        public MenuController backMenu;

		protected bool _initialized = false;
		protected MenuInterface _menuInterface;
		protected NavigationAnimator[] _navigations;
        protected CanvasGroup _canvasGroup;

        public virtual void Initialize (object data = null)
		{
            if (_initialized == true )
            {
                Refresh ( data );
                return;
            }

            _menuInterface = GetComponent<MenuInterface>();
            if (!_menuInterface) Debug.LogError ("No Menu Interface Found!");

            _canvasGroup = GetComponent<CanvasGroup>();
            if (!_canvasGroup) Debug.LogError ("No Canvas group found on View controller");


            _initialized = true;
            _menuInterface.Initialize( data );

            Refresh ( data );
		}

		public virtual bool HasInitialized ()
		{
			return _initialized;
		}

        public virtual void Refresh (object data = null )
        {
			_menuInterface.Refresh ( data );

			_navigations = GetComponentsInChildren < NavigationAnimator > (true);

			foreach (var n in _navigations)
			{
				n.Initialize();
			}
        }

        public virtual void SetInteractability (bool isInteractable)
        {
            if ( _canvasGroup == null )
            {
                _canvasGroup = GetComponent < CanvasGroup > ();
            }

            _canvasGroup.interactable = isInteractable;
        }

        public virtual float Enter ()
		{
            return _menuInterface.Enter();
		}

        public virtual float Exit ()
        {
            return _menuInterface.Exit();
        }

        public virtual void Reset ( bool control )
        {
            _menuInterface.Reset ( control );
        }

		public virtual Selectable[] GetSelectableComponents ()
		{
			return  GetComponentsInChildren < Selectable > (true);
		}

        public virtual void SetLastSelection ( Selectable selectable )
        {
            initiallyFocusedSelectable = selectable;
        }

		public virtual NavigationAnimator[] GetNavigationAnimators ()
		{
			return _navigations;
		}

        public virtual bool ContainsSelectable ( Selectable selection )
        {
			List<Selectable> selectables = GetSelectableComponents().ToList();
            return selectables.Contains ( selection );
        }

        public virtual Transition TryFindTransition ( MenuController controller )
        {
            Transition[] transitions = GetComponentsInChildren < Transition > (true);

            if ( transitions.Length < 1 ) return null;

            Transition transition = transitions.FirstOrDefault( x => x.nextMenu == controller );

            return transition;
        }

        public virtual void DoUpdate ()
        {
            // nothing goes into the update, override for frame control
        }

        public virtual float GetNavigationEnableDelay ( float defaultDelay )
        {
            if ( NavigationOption == NavigationOption.FixedTime )
            {
                return defaultNavigationEnableDelay;
            }

            if ( NavigationOption == NavigationOption.Initialization )
            {
                return 0f;
            }

            return defaultDelay;
        }

       
        public void Update ()
        {
            DoUpdate();
        }
	}
}
