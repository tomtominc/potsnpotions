﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using PotionSoup.Dispatcher;
using UnityEngine.Events;

namespace PotionSoup.MenuManagement
{
    public class MenuController_Selection : MenuController
    {
        [System.Serializable]
        public class InterfaceEvent : UnityEvent
        {

        }

        public InterfaceDatabase database;
        public GridLayoutGroup layoutGroup;
        public Transform selectionPrefab;

        public InterfaceEvent OnSelectInputPrimary = new InterfaceEvent();
        public InterfaceEvent OnSelectInputPrimaryAlt1 = new InterfaceEvent();
        public InterfaceEvent OnSelectInputPrimaryAlt2 = new InterfaceEvent();

        protected InterfaceObject _currentlySelectedItem;

        public override void Initialize(object data = null)
        {
            base.Initialize(database);
        }

        public override void Refresh(object data = null)
        {
            if (data == null)
                return;
            
            foreach (Transform t in layoutGroup.transform)
            {
                Destroy(t.gameObject);
            }

            database = data as InterfaceDatabase; 

            if (database == null)
                return;

            foreach (InterfaceObject interfaceObject in database.interfaceObjects)
            {
                CreateSelection(interfaceObject);
            }

            base.Refresh(data);
        }

        public override float Enter()
        {
            EventDispatcher.Subscribe < NavigationEvent >(NavigationEvent.OnSelectablePressed, null, OnNavigationButtonPressed, false);
            return base.Enter();
        }

        public override float Exit()
        {
            EventDispatcher.UnSubscribe < NavigationEvent >(NavigationEvent.OnSelectablePressed, null, OnNavigationButtonPressed, false);
            return base.Exit();
        }

        public virtual void OnNavigationButtonPressed(IMessage message)
        {
            Hashtable datatable = message.Data as Hashtable;

            if (datatable == null)
                return;

            Button button = datatable["Button"] as Button;
            bool positive = (bool)datatable["Positive"];
            bool positiveAlt1 = (bool)datatable["PositiveAlt1"];
            bool positiveAlt2 = (bool)datatable["PositiveAlt2"];

            if (button == null)
                return;

            InterfaceBinding binding = button.GetComponent < InterfaceBinding >();

            if (binding == null)
                return;

            _currentlySelectedItem = binding.objectData as InterfaceObject;


            if (positive == true)
            {
                OnSelectInputPrimary.Invoke();
            }
            else if (positiveAlt1 == true)
            {
                OnSelectInputPrimaryAlt1.Invoke();
            }
            else if (positiveAlt2 == true)
            {
                OnSelectInputPrimaryAlt2.Invoke();
            }
        }

        public virtual void CreateSelection(InterfaceObject item)
        {
            Transform clone = Instantiate< Transform >(selectionPrefab);
            clone.name = item.displayName;
            clone.SetParent(layoutGroup.transform, false);

            InterfaceBinding binding = clone.GetComponent < InterfaceBinding >();

            if (binding != null)
            {
                binding.BindData(item);
            }
        }
    }
}
