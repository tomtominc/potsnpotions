﻿using UnityEngine;
using System.Collections;
using PotionSoup.Input;
using PotionSoup.Dispatcher;
using UnityEngine.UI;

namespace PotionSoup.MenuManagement
{
    /// <summary>
    /// All the multi selection object does is handle the input state for 
    /// the game to make sure that all players will be able to control their various menus
    /// the rest of the functionality will be handled by PlayerControlledIntefaceItem
    /// this will allow the assigned player total control of the item and what it does.
    /// 
    /// </summary>
    public class MenuController_MultiSelection : MenuController
    {
        public InterfaceDatabase database;
        public GridLayoutGroup layoutGroup;
        public Transform selectionPrefab;

        public override void Initialize(object data = null)
        {
            base.Initialize(database);
        }

        public override void Refresh(object data = null)
        {
            if (data == null)
                return;
            
            database = data as InterfaceDatabase; 

            if (database == null)
                return;

            foreach (InterfaceObject interfaceObject in database.interfaceObjects)
            {
                CreateSelection(interfaceObject);
            }

            base.Refresh(data);
        }

        public virtual void CreateSelection(InterfaceObject item)
        {
            Transform clone = Instantiate< Transform >(selectionPrefab);
            clone.name = item.displayName;
            clone.SetParent(layoutGroup.transform, false);

            InterfaceBinding binding = clone.GetComponent < InterfaceBinding >();

            if (binding != null)
            {
                binding.BindData(item);
            }
        }

    }
}
