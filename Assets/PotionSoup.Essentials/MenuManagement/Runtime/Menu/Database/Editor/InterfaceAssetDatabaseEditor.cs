using UnityEditor;
using UnityEngine;
using System.Collections;
using EasyEditor;

namespace PotionSoup.MenuManagement
{
	[Groups("")]
	[CustomEditor(typeof(InterfaceAssetDatabase))]
	public class InterfaceAssetDatabaseEditor : EasyEditorBase
	{
	
	}
}