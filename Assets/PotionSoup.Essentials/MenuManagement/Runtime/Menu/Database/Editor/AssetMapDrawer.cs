﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using PotionSoup.Input;

namespace PotionSoup.MenuManagement
{
    [CustomPropertyDrawer (typeof(AssetMap))]
    [CustomPropertyDrawer (typeof(InputTypeAssetMap))]
    [CustomPropertyDrawer (typeof(TierPriceAssetMap))]
    public class AssetMapDrawer : PropertyDrawer 
    {
        public float propertyMultiplier = 3f;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * propertyMultiplier;
        }

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var categoryProp = property.FindPropertyRelative("category");
            var keyProp = property.FindPropertyRelative("key");
            var valueProp = property.FindPropertyRelative("value");

            EditorGUI.PropertyField(position,categoryProp,new GUIContent(categoryProp.displayName), true);

            position.y += 16f;

            EditorGUI.PropertyField(position,keyProp,new GUIContent(keyProp.displayName), true);

            position.y += 16f;

            EditorGUI.PropertyField(position,valueProp,new GUIContent(valueProp.displayName), true);

            EditorGUI.EndProperty();
        }
    }

    [CustomPropertyDrawer (typeof(AssetKey))]
    public class AssetKeyDrawer : PropertyDrawer 
    {
        public float propertyMultiplier = 1f;
        public int _currentAssetIndex = 0;
        
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * propertyMultiplier;
        }

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            InterfaceAssetDatabase assetDatabase = MenuManager.Instance.assetDatabase;

            var keyProp = property.FindPropertyRelative("key");

            string[] assets = null;

            if (assetDatabase != null)
            {
                assets = assetDatabase.GetAllAssetKeys ();

                if (assets.Length > 1 )
                {
					int index = Array.IndexOf(assets,keyProp.stringValue);

					if (index > -1) _currentAssetIndex = index;

                    _currentAssetIndex = EditorGUI.Popup (position,property.displayName, _currentAssetIndex, assets);

                    keyProp.stringValue = assets[_currentAssetIndex];


                }
            }

            EditorGUI.EndProperty();
        }
    }


}
