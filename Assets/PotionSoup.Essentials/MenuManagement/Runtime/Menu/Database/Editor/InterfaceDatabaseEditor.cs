﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Rewired.Editor.Libraries.Rotorz.ReorderableList;

namespace PotionSoup.MenuManagement
{
	[CustomEditor (typeof ( InterfaceDatabase ))]
	public class InterfaceDatabaseEditor : Editor 
	{
		private SerializedProperty interfaceObjectProp;

		public void OnEnable ()
		{
			interfaceObjectProp = serializedObject.FindProperty ("interfaceObjects");	
		}
		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector();

			serializedObject.Update();

			ReorderableListGUI.Title ("Interface Objects");
			ReorderableListGUI.ListField (interfaceObjectProp);

			serializedObject.ApplyModifiedProperties();
		}
	}
}
