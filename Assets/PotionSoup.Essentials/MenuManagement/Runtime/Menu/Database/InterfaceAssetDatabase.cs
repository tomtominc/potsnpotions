﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Input;
using System.Linq;

namespace PotionSoup.MenuManagement
{
	public enum TierPrice 
	{
		Tier1
	}

	public enum AssetCategory
	{
		Item, IAP, Input
	}

	public class InterfaceAssetDatabase : ScriptableObject
	{
		public static InterfaceAssetDatabase instance;

		public List < AssetMap > mappings = new List<AssetMap> ();
		public List < TierPriceAssetMap > tierPriceMappings = new List< TierPriceAssetMap > ();
        public List < InputTypeAssetMap > inputAssets = new List<  InputTypeAssetMap > ();

		protected Dictionary < string , Object > assetDatabase;

        public void Initialize ()
        {
            assetDatabase = new Dictionary<string, Object> ();
            assetDatabase.AddRange(GetValuesFromMapping < string > ( mappings.ConvertAll< AssetEnumMap < string > > ( x => x.Convert<string>() ) ) );
            assetDatabase.AddRange(GetValuesFromMapping < TierPrice > ( tierPriceMappings.ConvertAll< AssetEnumMap < TierPrice > > ( x => x.Convert<TierPrice>() ) ) );
            assetDatabase.AddRange(GetValuesFromMapping < InputType > ( inputAssets.ConvertAll< AssetEnumMap < InputType > > ( x => x.Convert<InputType>() ) ) );
        }


		public string[] GetAllAssetKeys ()
		{
			List < string > assetKeys = new List<string> ();

			foreach ( var pair in mappings )
			{
				assetKeys.Add (string.Format ("{0}/{1}",pair.category,pair.key));
			}

			foreach ( var pair in tierPriceMappings )
			{
				assetKeys.Add ( string.Format ("{0}/{1}",AssetCategory.IAP,pair.key));
			}

			return assetKeys.ToArray();
		}

        public Object GetAsset ( AssetKey key )
        {
            if (assetDatabase.ContainsKey (key.key))
            {
                return assetDatabase[key.key];
            }

            return null;
        }

        private List < string > GetKeysFromMapping < T > ( List < AssetEnumMap < T > > mapping )
        {
            List < string > temp = new List < string > ();
            foreach ( var map in mapping )
            {
                temp.Add(string.Format("{0}/{1}", map.category, map.key ) ); 
            }
            return temp;
        }

        private List < KeyValuePair < string, Object > > GetValuesFromMapping < T > ( List < AssetEnumMap < T > > mapping )
        {
            List < KeyValuePair < string, Object > > temp = new List < KeyValuePair <string, Object> > ();
            foreach ( var map in mapping )
            {
                var pair = new KeyValuePair<string, Object>( string.Format("{0}/{1}", map.category, map.key ), map.value );
                temp.Add( pair ); 
            }
            return temp;
        }

		
	}



    [System.Serializable]
    public class AssetEnumMap < T >
    {
        public AssetCategory category;
        public T key;
        public Object value;

        public AssetEnumMap < U > Convert < U > ()
        {
            return new AssetEnumMap< U > ()
            {
                category = this.category,
                key = this.key,
                value = this.value
            };
        }
    }

    [System.Serializable]
    public class AssetMap 
        : AssetEnumMap < string > {}

    [System.Serializable]
    public class InputTypeAssetMap 
        : AssetEnumMap < InputType > {}

	[System.Serializable]
    public class TierPriceAssetMap 
        : AssetEnumMap < TierPrice > {}

    [System.Serializable]
    public class AssetKey
    {
        public string key;

        public AssetKey ( AssetCategory category, object key )
        {
            this.key = string.Format("{0}/{1}", category,key);
        }
    }
}
