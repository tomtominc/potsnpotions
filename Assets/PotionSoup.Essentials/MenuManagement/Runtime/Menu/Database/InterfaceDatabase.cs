﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PotionSoup.MenuManagement
{
    public class InterfaceDatabase : ScriptableObject
    {
        public string DatabaseId = "Interface Database";
        [HideInInspector]
        public List < InterfaceObject > interfaceObjects = new List<InterfaceObject> ();

        public virtual InterfaceObject EnableObject (int index)
        {
            int originalIndex = index;
            InterfaceObject databaseObject = null;

            do {
                if (interfaceObjects.Count < index && index >= 0) {
                    var interfaceObject = interfaceObjects [index];

                    if (interfaceObject == null) return null;

                    if (interfaceObject.enabled)
                    {
                        index = index.GetWrapped (0, Count ()-1, 1);
                    }
                    else
                    {
                        interfaceObject.enabled = true;
                        databaseObject = interfaceObject;
                        break;
                    }
                }
            } while (index != originalIndex);

            if (databaseObject == null && interfaceObjects.Count < originalIndex && originalIndex >= 0)
            {
                databaseObject = interfaceObjects[ originalIndex ];
            }

            return databaseObject;
           
        }

        public virtual void DisableObject (int index)
        {
            
        }

        public virtual InterfaceObject GetObjectbyIndex (int index)
        {
            if (interfaceObjects.Count > index && index >= 0) 
            {
                return interfaceObjects[index];
            }

            Debug.LogWarning("Trying to retrieve an invalid interface object at index: " + index);

            return null;
        }

        public virtual int Count ()
        {
            return interfaceObjects.Count;
        }
    }
}
