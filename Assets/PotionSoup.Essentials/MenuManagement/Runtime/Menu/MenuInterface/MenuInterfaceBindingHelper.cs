﻿using UnityEngine;
using System.Collections;

namespace PotionSoup.MenuManagement
{
    public class MenuInterfaceBindingHelper : MenuInterface 
    {
        public InterfaceBinding binding;
        public InterfaceObject injectedData;

        public override void Initialize(object data)
        {
            base.Initialize(data);

            if ( data == null && injectedData != null )
            {
                binding.BindData(injectedData);
            }
            else 
            {
                binding.BindData(data);
            }


        }
    }
}
