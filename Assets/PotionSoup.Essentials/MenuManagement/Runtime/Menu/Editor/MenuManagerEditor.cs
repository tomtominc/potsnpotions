using UnityEditor;
using UnityEngine;
using System.Collections;
using EasyEditor;

namespace PotionSoup.MenuManagement
{
	[Groups("General Settings")]
	[CustomEditor(typeof(MenuManager))]
	public class MenuManagerEditor : EasyEditorBase
	{
	    
	}
}