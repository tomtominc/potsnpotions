﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[System.Serializable]
public class DoTweenBase 
{
    protected Tweener _tweener;

    public bool autoPlay = true;
    public float duration = 1f;
    public float delay = 1f;
    public bool ignoreTimescale = false;
    public Ease ease;
    public int loops = 1;
    public LoopType loopType;
    public string id;
    public bool to = false;


    public virtual void Initialize ( Object component )
    {

    }

    public virtual void Play ()
    {

    }

    public virtual void Pause ()
    {

    }

    public virtual void Restart ()
    {

    }
}
