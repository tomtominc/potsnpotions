﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DoScaleTween : DoTweenBase 
{
    protected RectTransform _rectTransform;

    public Vector3 endValue = Vector3.one;
    public bool relative = false;

    public override void Initialize (Object component)
    {
        _rectTransform = (RectTransform)component;
    }

    public override void Play ()
    {
        _tweener = _rectTransform.DOScale ( endValue, duration );
        _tweener.SetDelay(delay);
        _tweener.SetAutoKill(false);
        _tweener.SetEase(ease);
        _tweener.SetId(id);
        _tweener.SetLoops(loops,loopType);
        _tweener.SetRelative(relative);

        _tweener.Play();
    }

    public override void Restart ()
    {
        _tweener.Restart(true);
    }

    public override void Pause ()
    {
        _tweener.Pause();
    }
}
