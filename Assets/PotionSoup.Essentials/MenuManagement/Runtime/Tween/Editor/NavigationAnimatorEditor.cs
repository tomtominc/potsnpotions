﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Smart.Types;
using DG.Tweening;

namespace PotionSoup.MenuManagement
{
	[CustomEditor(typeof(NavigationAnimator))]
	public class NavigationAnimatorEditor : Editor 
	{

		private NavigationAnimator navigationAnimator;
		private int header_selection = 0;

		private bool foldout = false;

		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector();

			navigationAnimator = target as NavigationAnimator;
			header_selection = EditorGUIExtensions.DrawSelectableHeader ( "Navigation_Animator_Header" , new string[]  { "On Focus Tweens" , "On Lose Focus Tweens" } );

			EditorGUIExtensions.BeginContent(Color.white);
			switch ( header_selection )
			{
			case 0: DrawOnFocusedParams(); break;
			case 1: DrawOnLoseFocusedParams (); break;
			}
			EditorGUIExtensions.EndContent();

		}

		private void DrawAnimatorParams (TweenData animatorParams , Color aColor, string paramsName )
		{
			if (animatorParams == null ) return;

			aColor.a = animatorParams.isOn ? aColor.a : aColor.a * 0.2f;


			foldout = EditorGUIExtensions.DrawFoldout(foldout,aColor,paramsName, paramsName);


			if ( foldout == true)
			{
				EditorGUIExtensions.BeginContent(new Color(1f,1f,1f));
				animatorParams.isOn = EditorGUILayout.BeginToggleGroup("Enabled",animatorParams.isOn);


				animatorParams.duration = EditorGUILayout.FloatField ( "Duration" , animatorParams.duration );
				animatorParams.delay =  EditorGUILayout.FloatField ( "Delay" , animatorParams.delay );
				animatorParams.loops =  EditorGUILayout.IntField ( "Loops" , animatorParams.loops );
				animatorParams.loopType = (LoopType)EditorGUILayout.EnumPopup ("LoopType", animatorParams.loopType);
				animatorParams.ease = (Ease)EditorGUILayout.EnumPopup ( "Ease" , animatorParams.ease );
				animatorParams.cullingEffect = (TimingAction)EditorGUILayout.EnumPopup("When To Do Culling", animatorParams.cullingEffect);
				animatorParams.cullingMode = (CullingMode)EditorGUILayout.EnumPopup ("Culling Mode", animatorParams.cullingMode);

				EditorGUILayout.BeginHorizontal ();

				string destination = animatorParams.isFrom ? "From" : "To";


				if ( GUILayout.Button ( destination , GUILayout.Width (48)))
				{
					animatorParams.isFrom = animatorParams.isFrom.Toggle();
				}

				animatorParams.from = EditorGUILayout.Vector3Field ( "" , animatorParams.from );

				if ( GUILayout.Button ( "Copy" , GUILayout.Width (48)))
				{
					animatorParams.from = navigationAnimator.GetComponent < RectTransform> ().localPosition;
				}

				EditorGUILayout.EndHorizontal();
				animatorParams.beginSound = ( AudioClip )EditorGUILayout.ObjectField ( "Begin Sound" , animatorParams.beginSound , typeof ( AudioClip ) , true );
				animatorParams.endSound =( AudioClip ) EditorGUILayout.ObjectField ( "End Sound" , animatorParams.endSound , typeof ( AudioClip ) , true );

				if ( paramsName == "Rotate In" )
				{
					animatorParams.rotationMode = (RotateMode) EditorGUILayout.EnumPopup ( "Rotation Mode" , animatorParams.rotationMode );
				}

				EditorGUILayout.EndToggleGroup();
				EditorGUIExtensions.EndContent();
			}

		}

		private void DrawOnFocusedParams ()
		{
			DrawAnimatorParams ( navigationAnimator.MoveOnFocus ,EditorColor.yellow, "Move On Focus" + navigationAnimator.GetUniqueGuid() );
            DrawAnimatorParams ( navigationAnimator.ScaleOnFocus ,EditorColor.green, "Scale On Focus" + navigationAnimator.GetUniqueGuid());
            DrawAnimatorParams ( navigationAnimator.RotateOnFocus , EditorColor.red, "Rotate On Focus" + navigationAnimator.GetUniqueGuid());
		}

		private void DrawOnLoseFocusedParams ()
		{
            DrawAnimatorParams ( navigationAnimator.MoveOnLoseFocus ,EditorColor.yellow, "Move On Lose Focus" + navigationAnimator.GetUniqueGuid());
            DrawAnimatorParams ( navigationAnimator.ScaleOnLoseFocus ,EditorColor.green, "Scale On Lose Focus"+ navigationAnimator.GetUniqueGuid());
            DrawAnimatorParams ( navigationAnimator.RotateOnLoseFocus , EditorColor.red, "Rotate On Lose Focus"+ navigationAnimator.GetUniqueGuid());
		}
	}
}
