using UnityEngine;
using UnityEditor;
using System.Collections;
using DG.Tweening;
using Smart.Types;

namespace PotionSoup.MenuManagement
{
	[CustomEditor ( typeof ( TweenAnimator )) ]
	public class TweenAnimationEditor : Editor 
	{
		private TweenAnimator tweenAnimator;
		private int header_selection = 0;

		private bool foldout = false;
              
		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector();

			tweenAnimator = target as TweenAnimator;
			header_selection = EditorGUIExtensions.DrawSelectableHeader ( "Tween_Animator_Header" , new string[]  { "Enter Tweens" , "Idle Tweens" , "Exit Tweens" } );

			EditorGUIExtensions.BeginContent(Color.white);
			switch ( header_selection )
			{
			case 0: DrawInParams(); break;
			case 1: DrawIdleParams (); break;
			case 2: DrawOutParams(); break;
			}
			EditorGUIExtensions.EndContent();

		}

		private void DrawAnimatorParams (TweenData animatorParams , Color aColor, string paramsName, string destination = "From" )
		{
			if (animatorParams == null ) return;

			aColor.a = animatorParams.isOn ? aColor.a : aColor.a * 0.2f;


			foldout = EditorGUIExtensions.DrawFoldout(foldout,aColor,paramsName, paramsName);


			if ( foldout == true)
			{
				EditorGUIExtensions.BeginContent(new Color(1f,1f,1f));
				animatorParams.isOn = EditorGUILayout.BeginToggleGroup("Enabled",animatorParams.isOn);

				animatorParams.duration = EditorGUILayout.FloatField ( "Duration" , animatorParams.duration );
				animatorParams.delay =  EditorGUILayout.FloatField ( "Delay" , animatorParams.delay );
				animatorParams.loops =  EditorGUILayout.IntField ( "Loops" , animatorParams.loops );
				animatorParams.ease = (Ease)EditorGUILayout.EnumPopup ( "Ease" , animatorParams.ease );

				EditorGUILayout.BeginHorizontal ();
				animatorParams.from = EditorGUILayout.Vector3Field ( destination , animatorParams.from );
				
				if ( GUILayout.Button ( "Copy" , GUILayout.Width (48)))
				{
					animatorParams.from = tweenAnimator.GetComponent < RectTransform> ().localPosition;
				}

				EditorGUILayout.EndHorizontal();
				animatorParams.beginSound = ( AudioClip )EditorGUILayout.ObjectField ( "Begin Sound" , animatorParams.beginSound , typeof ( AudioClip ) , true );
				animatorParams.endSound =( AudioClip ) EditorGUILayout.ObjectField ( "End Sound" , animatorParams.endSound , typeof ( AudioClip ) , true );

				if ( paramsName == "Rotate In" )
				{
					animatorParams.rotationMode = (RotateMode) EditorGUILayout.EnumPopup ( "Rotation Mode" , animatorParams.rotationMode );
				}

				EditorGUILayout.EndToggleGroup();
				EditorGUIExtensions.EndContent();
			}

		}

		private void DrawInParams ()
		{
			DrawAnimatorParams ( tweenAnimator.MoveIn ,EditorColor.yellow, "Move In" );
            DrawAnimatorParams ( tweenAnimator.ScaleIn ,EditorColor.green, "Scale In" );
            DrawAnimatorParams ( tweenAnimator.RotateIn , EditorColor.red, "Rotate In" );
		}

		private void DrawIdleParams ()
		{
            DrawAnimatorParams ( tweenAnimator.MoveIdle ,EditorColor.yellow, "Move Idle" );
            DrawAnimatorParams ( tweenAnimator.ScaleIdle ,EditorColor.green, "Scale Idle" );
            DrawAnimatorParams ( tweenAnimator.RotateIdle , EditorColor.red, "Rotate Idle" );
		}

		private void DrawOutParams ()
		{
            DrawAnimatorParams ( tweenAnimator.MoveOut ,EditorColor.yellow, "Move Out" , "To" );
            DrawAnimatorParams ( tweenAnimator.ScaleOut ,EditorColor.green, "Scale Out" , "To" );
            DrawAnimatorParams ( tweenAnimator.RotateOut , EditorColor.red, "Rotate Out" , "To" );
		}
	}
}
