﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using PotionSoup.Dispatcher;
using UnityEngine.UI;

namespace PotionSoup.MenuManagement
{
	public class NavigationEventListener : MonoBehaviour 
	{
		public Selectable selectingComponent;
		public UnityEvent NavigationFocus = new UnityEvent();
		public UnityEvent NavigationLoseFocus = new UnityEvent();

		public void Start ()
		{
			EventDispatcher.Subscribe < NavigationEvent > ( NavigationEvent.OnFocus, selectingComponent.GetUniqueGuid(), OnNavigationFocus,true );
			EventDispatcher.Subscribe < NavigationEvent > ( NavigationEvent.OnLoseFocus, selectingComponent.GetUniqueGuid(), OnNavigationLoseFocus, true);
		}

		public void OnNavigationFocus ( IMessage message )
		{
			NavigationFocus.Invoke();
		}

		public void OnNavigationLoseFocus ( IMessage message )
		{
			NavigationLoseFocus.Invoke();
		}
	}
}
