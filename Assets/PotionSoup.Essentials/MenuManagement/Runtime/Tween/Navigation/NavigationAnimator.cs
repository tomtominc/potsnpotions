﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PotionSoup.Dispatcher;
using UnityEngine.UI;
using System;

namespace PotionSoup.MenuManagement
{
	public class NavigationAnimator : MonoBehaviour
	{
		public bool deactivateAfterLoseFocus = true;
		public bool resetAfterLoseFocus = true;

		[HideInInspector]
		public TweenData MoveOnFocus;

		[HideInInspector]
		public TweenData RotateOnFocus;

		[HideInInspector]
		public TweenData ScaleOnFocus;

		[HideInInspector]
		public TweenData MoveOnLoseFocus;

		[HideInInspector]
		public TweenData RotateOnLoseFocus;

		[HideInInspector]
		public TweenData ScaleOnLoseFocus;

		private RectTransform _rectTransform;

		private Vector3 _originalPosition;
		private Vector3 _originalScale;
		private Vector3 _originalRotation;

		private Sequence _onFocusSequence;
		private Sequence _onLoseFocusSequence;

		private bool _isFocusComplete = false;
		private bool _isLoseFocusComplete = false;

		protected Selectable _navigationSelectable;

		public void Initialize ()
		{
			_navigationSelectable = gameObject.GetComponentInParent< Selectable > (true,false);// GetComponentInParent < Selectable > ();

			if (_navigationSelectable == null) Debug.Log ( "No Selectable at the top level: " + this.name + " " + typeof (NavigationAnimator));

			_rectTransform = GetComponent<RectTransform> ();
			_originalPosition = _rectTransform.localPosition;
			_originalScale = _rectTransform.localScale;
			_originalRotation = _rectTransform.localEulerAngles;

			EventDispatcher.Subscribe < NavigationEvent > ( NavigationEvent.OnFocus, _navigationSelectable.GetUniqueGuid(), OnFocus,true );
			EventDispatcher.Subscribe < NavigationEvent > ( NavigationEvent.OnLoseFocus, _navigationSelectable.GetUniqueGuid(), OnLoseFocus, true);
		}

		protected void OnFocus (IMessage message)
		{
			if (!gameObject.IsActive()) gameObject.SetActive(true);
			KillSequences();
			DoFocusTween();
		}

		protected void OnLoseFocus (IMessage message)
		{
			if (!gameObject.IsActive()) gameObject.SetActive(true);
			
			KillSequences();
			DoLoseFocusTween();
		}

		private void KillSequences ()
		{
			if (_onLoseFocusSequence != null)
			{
				_onLoseFocusSequence.Kill (true);
				_onLoseFocusSequence = null;
			}
			if (_onFocusSequence != null)
			{
				_onFocusSequence.Kill (true);
				_onFocusSequence = null;
			}
		}

		public void ResetAnimator ()
		{
			_rectTransform.localPosition = _originalPosition;
			_rectTransform.localScale = _originalScale;
			_rectTransform.localEulerAngles = _originalRotation;
		}

		protected virtual void OnFocusCompleted ()
		{
			_isFocusComplete = true;
		}
		protected virtual void OnLoseFocusCompleted ()
		{
			if (deactivateAfterLoseFocus) gameObject.SetActive(false);

			if (resetAfterLoseFocus) ResetAnimator();

			_isLoseFocusComplete = true;
		}


		public bool IsFocusComplete ()
		{
			return _isFocusComplete;
		}
			
		public bool IsOnLoseFocusComplete ()
		{
			return _isLoseFocusComplete;
		}

		protected virtual void PlaySound (AudioClip clip)
		{
			if (clip == null) {
				return;
			}

			AudioSource.PlayClipAtPoint (clip, Vector3.zero, 1f);
		}

		public virtual void DoCulling ( CullingMode mode )
		{
			switch (mode)
			{
			case CullingMode.Front: transform.SetAsLastSibling ();break;
			case CullingMode.Back: transform.SetAsFirstSibling(); break;
			}
		}

		public float DoFocusTween ()
		{
			_onFocusSequence = DOTween.Sequence ();

			if (MoveOnFocus.isOn) 
			{
				Tweener  tween = _rectTransform.DOLocalMove (MoveOnFocus.from, MoveOnFocus.duration, false).
					SetEase (MoveOnFocus.ease).
					SetLoops (MoveOnFocus.loops, MoveOnFocus.loopType).
					SetDelay (MoveOnFocus.delay);

				if (MoveOnFocus.isFrom ) tween.From();

				_onFocusSequence.Append (tween);
				_onFocusSequence.OnStart (() => 
					{
						PlaySound (MoveOnFocus.beginSound);

						if (MoveOnFocus.cullingEffect == TimingAction.OnStart)
						{
							DoCulling(MoveOnFocus.cullingMode);
						}
					});
				
				_onFocusSequence.OnComplete (() => 
					{
						PlaySound (MoveOnFocus.endSound);

						if (MoveOnFocus.cullingEffect == TimingAction.OnEnd)
						{
							DoCulling(MoveOnFocus.cullingMode);
						}

						OnFocusCompleted ();
					});

			}

			if (RotateOnFocus.isOn) {

				Tweener tween =  _rectTransform.DOLocalRotate (RotateOnFocus.from, RotateOnFocus.duration, RotateOnFocus.rotationMode).
					SetEase (RotateOnFocus.ease).
					SetLoops (RotateOnFocus.loops).
					SetDelay (RotateOnFocus.delay);

				if (RotateOnFocus.isFrom ) tween.From();

				_onFocusSequence.Insert (0f,tween);

				_onFocusSequence.OnStart (() => PlaySound (RotateOnFocus.beginSound));
				_onFocusSequence.OnComplete (() => {
					PlaySound (RotateOnFocus.endSound);
					OnFocusCompleted ();
				});
			}

			if (ScaleOnFocus.isOn) {

				Tweener tween = _rectTransform.DOScale (ScaleOnFocus.from, ScaleOnFocus.duration).
					SetEase (ScaleOnFocus.ease).
					SetLoops (ScaleOnFocus.loops).
					SetDelay (ScaleOnFocus.delay);

				tween.SetRecyclable(true);

				if (ScaleOnFocus.isFrom ) tween.From();

				_onFocusSequence.Insert (0f, tween);

				_onFocusSequence.OnStart (() => PlaySound (ScaleOnFocus.beginSound));
				_onFocusSequence.OnComplete (() => {
					PlaySound (ScaleOnFocus.endSound);
					OnFocusCompleted ();
				});
			}

			_onFocusSequence.Play ();

			return _onFocusSequence.Duration ();

		}

		public float DoLoseFocusTween ()
		{
			_onLoseFocusSequence = DOTween.Sequence ();

			if (MoveOnLoseFocus.isOn) 
			{
				Tweener tween = _rectTransform.DOLocalMove (MoveOnLoseFocus.from, MoveOnLoseFocus.duration, false).
					SetEase (MoveOnLoseFocus.ease).
					SetLoops (MoveOnLoseFocus.loops, MoveOnLoseFocus.loopType).
					SetDelay (MoveOnLoseFocus.delay);

				if (MoveOnLoseFocus.isFrom) tween.From();

				_onLoseFocusSequence.Append (tween);

				_onLoseFocusSequence.OnStart (() =>  
					{ 
						PlaySound (MoveOnLoseFocus.beginSound);
						if (MoveOnLoseFocus.cullingEffect == TimingAction.OnStart)
						{
							DoCulling(MoveOnLoseFocus.cullingMode);
						}
					});
				
				_onLoseFocusSequence.OnComplete (() => 
					{
						PlaySound (MoveOnLoseFocus.endSound);
						OnLoseFocusCompleted ();

						if (MoveOnLoseFocus.cullingEffect == TimingAction.OnEnd)
						{
							DoCulling(MoveOnLoseFocus.cullingMode);
						}
					});
			}

			if (RotateOnLoseFocus.isOn) {

				Tweener tween = _rectTransform.DOLocalMove (MoveOnLoseFocus.from, MoveOnLoseFocus.duration, false).
					SetEase (MoveOnLoseFocus.ease).
					SetLoops (MoveOnLoseFocus.loops).
					SetDelay (MoveOnLoseFocus.delay);

				if (RotateOnLoseFocus.isFrom) tween.From();

				_onLoseFocusSequence.Insert (0f, tween);

				_onLoseFocusSequence.OnStart (() => PlaySound (RotateOnLoseFocus.beginSound));
				_onLoseFocusSequence.OnComplete (() => {
					PlaySound (RotateOnLoseFocus.endSound);
					OnLoseFocusCompleted ();
				});
			}

			if (ScaleOnLoseFocus.isOn) {

				Tweener tween = _rectTransform.DOScale (ScaleOnLoseFocus.from, ScaleOnLoseFocus.duration).
					SetEase (ScaleOnLoseFocus.ease).
					SetLoops (ScaleOnLoseFocus.loops).
					SetDelay (ScaleOnLoseFocus.delay);

				if (ScaleOnLoseFocus.isFrom) tween.From();
				_onLoseFocusSequence.Insert (0f, tween);

				_onLoseFocusSequence.OnStart (() => PlaySound (ScaleOnLoseFocus.beginSound));
				_onLoseFocusSequence.OnComplete (() => {
					PlaySound (ScaleOnLoseFocus.endSound);
					OnLoseFocusCompleted ();
				});
			}

			_onLoseFocusSequence.Play ();

			return _onLoseFocusSequence.Duration ();
		}

	}
}
