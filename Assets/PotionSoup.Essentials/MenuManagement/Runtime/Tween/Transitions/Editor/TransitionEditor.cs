using UnityEditor;
using UnityEngine;
using System.Collections;
using EasyEditor;

namespace PotionSoup.MenuManagement
{
	[Groups( "Enter Settings", "Exit Settings" )]
	[CustomEditor(typeof(Transition),true)]
	public class TransitionEditor : EasyEditorBase
	{
	
	}
}