﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using EasyEditor;

namespace PotionSoup.MenuManagement
{
    public enum NextMenuEnterBehaviour
    {
        Immediately, AfterCloseFinish,  FixedTime
    }

    public enum ExitBehaviour
    {
        Disabled, ExitOnly, ExitAndOpenNextMenu, OpenNextMenu
    }

	public class Transition : MonoBehaviour 
	{
        public ExitBehaviour ExitBehaviour = ExitBehaviour.Disabled;

        [Visibility ( "ExitBehaviour", ExitBehaviour.ExitAndOpenNextMenu, ExitBehaviour.OpenNextMenu)]
        public NextMenuEnterBehaviour NextMenuEnterBehaviour = NextMenuEnterBehaviour.Immediately;

        [Visibility ( "ExitBehaviour", ExitBehaviour.ExitAndOpenNextMenu, ExitBehaviour.OpenNextMenu)]
        public MenuController nextMenu;

        [Visibility ( "NextMenuEnterBehaviour",NextMenuEnterBehaviour.FixedTime )]
        public float nextMenuDelay;

        protected Button _button;

		private void Start ()
		{
			_button = GetComponent<Button>();
			_button.onClick.AddListener ( StartTransiton );
		}

        public virtual void StartTransiton ()
        {
            MenuManager.Instance.OpenMenu ( this );
		}

        public virtual float GetEnterDelay ( float defaultDelay )
        {

            if ( NextMenuEnterBehaviour == NextMenuEnterBehaviour.Immediately )
            {
                return 0f;
            }

            if ( NextMenuEnterBehaviour == NextMenuEnterBehaviour.FixedTime )
            {
                return nextMenuDelay;
            }

            return defaultDelay;
        }

	}
}
