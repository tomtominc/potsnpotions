﻿using UnityEngine;
using System.Collections;
using Rewired;
using PotionSoup.Input;

namespace PotionSoup.MenuManagement
{
	public class Transition_Input : Transition 
	{
		public string navigationInvokeKey = StringManager.input_menu_right_alternate;
		protected Player _player;

		private void Start ()
		{
			_player = InputBridge.GetSystemPlayer();
		}

		public void Update ()
		{
			bool invokeKey = _player.GetButtonDown (navigationInvokeKey);

			if (invokeKey == true)
			{
                StartTransiton();
			}
		}

	}
}
