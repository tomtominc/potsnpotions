﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Rewired;
using PotionSoup.Input;
using PotionSoup.Dispatcher;

namespace PotionSoup.MenuManagement
{
	public class NavigationManagement : MonoBehaviour
	{
		public NavigationCursor cursor;

		protected float sensitivity = 0.1f;

		protected MenuController _controller;

		protected Selectable _currentlySelected;
        protected Selectable _lastSelection;
        protected bool _saveLastSelection = false;

		protected EventSystem _system;
		protected Player _player;
		protected bool _hasReset = true;

		public bool isEnabled = false;

		public void Initialize ()
		{
			_player = InputBridge.GetSystemPlayer ();
			_system = EventSystem.current;

			if (_player == null) Debug.LogError ("Player is null " + _player);

            EventDispatcher.Subscribe < InputEvent > ( InputEvent.OnChangeSystemPlayer , null, OnChangedSystemPlayer, true);
		}

		public void OnChangedSystemPlayer (IMessage message)
		{
			_player = InputBridge.GetSystemPlayer ();
		}

		public void OnChangedModal (IMessage message)
		{
			Hashtable datatable = message.Data as Hashtable;
			MenuController controller = datatable ["Modal"] as MenuController;

			if (controller == _controller) return;

            EnableNavigation ( controller );
		}

      

		public Selectable GetCurrentSelectable ()
		{
			return _currentlySelected;
		}

		public InterfaceObject GetCurrentInterfaceObject ()
		{
			if (_currentlySelected == null) return null;

            InterfaceBinding interfaceBinding = _currentlySelected.GetComponent < InterfaceBinding > ();

			if (interfaceBinding == null ) return null;

            InterfaceObject interfaceObject = interfaceBinding.objectData as InterfaceObject;

			return interfaceObject;
		}

		public InterfaceBinding GetCurrentInterfaceBinding ()
		{
			if (_currentlySelected == null) return null;

			var interfaceBinding = _currentlySelected.GetComponent < InterfaceBinding > ();

			return interfaceBinding;
		}

		public virtual void DisableCursor ()
		{
			if ( cursor != null )
			{
				cursor.gameObject.SetActive ( false );
			}
		}

		public virtual void DisableNavigation (bool saveLastSelection)
		{
			if ( saveLastSelection )
			{
				_lastSelection = _currentlySelected;
			}

			isEnabled = false;

			_saveLastSelection = saveLastSelection;

			SetSelected (null);

			DisableCursor ();

		}

        public virtual void EnableNavigation ( MenuController controller )
        {
			if (isEnabled)
			{
				cursor.Refresh ();
			}

			isEnabled = true;

            _controller = controller;

			Selectable next = null;

           if ( _controller == null ) return;

            if ( _saveLastSelection && _controller.ContainsSelectable ( _lastSelection ) )
            {
                next = _lastSelection;
                _saveLastSelection = false;
                _lastSelection = null;
            }

			if (next == null)
			{
            	next = _controller.initiallyFocusedSelectable;
			}

            if ( next == null)
            {
                var components = _controller.GetSelectableComponents();

                if ( components.Length > 0 )
                {
                    next = components[0];
                }
            }

            SetSelected (next);
        }

		private void Update ()
		{
			UpdateSelection();
			UpdateClick();
		}

		private void SetSelected (Selectable selectable)
		{

			if (_currentlySelected != null)
			{
				EventDispatcher.Publish < NavigationEvent > ( this,NavigationEvent.OnLoseFocus,_currentlySelected.GetUniqueGuid(), null);
			}

			_currentlySelected = selectable;

			if (_currentlySelected == null)
            {
                cursor.SetSelectable(null);
                return;
            }

			_system.SetSelectedGameObject (selectable.gameObject);

			_currentlySelected.Select ();

			EventDispatcher.Publish < NavigationEvent > ( this, NavigationEvent.OnFocus , _currentlySelected.GetUniqueGuid(), null);

			InputField field = _currentlySelected as InputField;

			if (field != null) 
			{
				field.OnPointerClick (new PointerEventData (_system));
			}

			if (cursor != null)
			{
				cursor.SetSelectable(_currentlySelected);
			}

            if ( _controller != null )
            {
                _controller.SetLastSelection(_currentlySelected);
            }
		}

		private void UpdateSelection ()
		{
            if (_currentlySelected == null ) return;

			float vert = _player.GetAxisRaw (StringManager.input_menu_vertical);
			float horz = _player.GetAxisRaw (StringManager.input_menu_horizontal);

			if (Mathf.Abs (vert) < sensitivity && Mathf.Abs (horz) < sensitivity) 
			{
				_hasReset = true;
			}

			if ((Mathf.Abs (vert) > sensitivity || Mathf.Abs (horz) > sensitivity) && _hasReset) 
			{
				_hasReset = false;
				Selectable next = null;

				if (Mathf.Abs (horz) != 0) 
				{
					if (horz > sensitivity) 
					{
						next = _currentlySelected.FindSelectableOnRight ();
					}
					if (horz < sensitivity) 
					{
						next = _currentlySelected.FindSelectableOnLeft ();
					}
				}
				if (Mathf.Abs (vert) != 0) 
				{
					if (vert > sensitivity) 
					{
						next = _currentlySelected.FindSelectableOnUp ();
					}
					if (vert < sensitivity) 
					{
						next = _currentlySelected.FindSelectableOnDown ();
					}
				}

				if (next != null) 
				{
					SetSelected(next);
				}

			}
		}

		private void UpdateClick ()
		{
            bool menu_positive = _player.GetButtonDown(StringManager.input_menu_positive);
			bool menu_positive_alt1 = _player.GetButtonDown (StringManager.input_menu_positive_alt1);
			bool menu_positive_alt2 = _player.GetButtonDown (StringManager.input_menu_positive_alt2);
			bool menu_negative = _player.GetButtonDown(StringManager.input_menu_negative);

			var pointer = new PointerEventData(EventSystem.current);

			if (menu_positive || menu_positive_alt1 || menu_positive_alt2)
			{
                EventDispatcher.Publish < NavigationEvent >  ( this, NavigationEvent.OnPositive , null, null );

                if (_currentlySelected == null ) return;

				Button button = _currentlySelected.GetComponent<Button>();
				if (button != null)
				{
					ExecuteEvents.Execute(button.gameObject, pointer, ExecuteEvents.submitHandler);

                    var datatable = new Hashtable ()
                    {
                        { "Button" , button },
						{ "Positive", menu_positive},
						{ "PositiveAlt1", menu_positive_alt1 },
						{ "PositiveAlt2", menu_positive_alt2 }

                    };

					EventDispatcher.Publish < NavigationEvent > ( this, NavigationEvent.OnSelectablePressed , null, datatable);
				}
			}
			else if (menu_negative)
			{
                MenuManager.Instance.OpenPreviousMenu();

                EventDispatcher.Publish < NavigationEvent > ( this, NavigationEvent.OnNegative , null, null );
			}

		}


	}
}
