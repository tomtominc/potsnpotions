﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RektTransform;

namespace PotionSoup.MenuManagement
{
	public class NavigationCursor : MonoBehaviour
	{
		protected Selectable _currentSelection;
        protected RectTransform _currentRect 
        { 
            get { return _currentSelection.GetComponent<RectTransform> (); } 
        }

		public virtual void Refresh ()
		{
            transform.SetAsLastSibling();

			SetSelectable (_currentSelection);
		}

		public virtual void SetSelectable (Selectable selection)
        {
			if (!gameObject.IsActive())
			{
				gameObject.SetActive(true);
			}
			_currentSelection = selection;
		}

		protected virtual float GetWidth ()
		{
			return _currentRect.rect.width;
		}

		protected virtual float GetHeight ()
		{
			return _currentRect.rect.height;
		}

		protected virtual Vector2 GetPosition ()
        {
            Vector2 origin = _currentRect.localPosition - ( -1 * _currentRect.GetParent().localPosition);
            return origin;
		}
	}
}
