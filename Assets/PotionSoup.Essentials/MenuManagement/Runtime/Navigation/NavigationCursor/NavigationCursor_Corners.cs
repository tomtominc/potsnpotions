﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

namespace PotionSoup.MenuManagement
{
	public class NavigationCursor_Corners : NavigationCursor
	{
		float tweenOffset = 2f;
		float tweenSpeed = 0.1f;

		public RectTransform upperRight;
		public RectTransform upperLeft;
		public RectTransform lowerRight;
		public RectTransform lowerLeft;

		private float _offset = 0.5f;
		private Tween _URTween;
		private Tween _ULTween;
		private Tween _LRTween;
		private Tween _LLTween;



		public override void SetSelectable (Selectable selection)
		{
			base.SetSelectable (selection);

            if (_currentSelection == null)
            {
                gameObject.SetActive(false); 
                return;
            }

			if (_URTween != null)
			{
				_URTween.Kill();
				_ULTween.Kill();
				_LRTween.Kill();
				_LLTween.Kill();
			}

			upperRight.localPosition = GetRightUpperCorner();
			upperLeft.localPosition  = GetLeftUpperCorner();
			lowerRight.localPosition = GetRightLowerCorner();
			lowerLeft.localPosition = GetLeftLowerCorner();

			_URTween = upperRight.DOLocalMove( new Vector3(upperRight.localPosition.x - tweenOffset, 
				upperRight.localPosition.y - tweenOffset),
				tweenSpeed,false).SetLoops(-1,LoopType.Yoyo);

			_ULTween = upperLeft.DOLocalMove( new Vector3(upperLeft.localPosition.x + tweenOffset, 
				upperLeft.localPosition.y - tweenOffset),
				tweenSpeed,false).SetLoops(-1,LoopType.Yoyo);

			_LRTween = lowerRight.DOLocalMove( new Vector3(lowerRight.localPosition.x - tweenOffset, 
				lowerRight.localPosition.y + tweenOffset),
				tweenSpeed,false).SetLoops(-1,LoopType.Yoyo);

			_LLTween = lowerLeft.DOLocalMove( new Vector3(lowerLeft.localPosition.x + tweenOffset, 
				lowerLeft.localPosition.y + tweenOffset),
				tweenSpeed,false).SetLoops(-1,LoopType.Yoyo);

		}

		public virtual Vector3 GetRightUpperCorner ()
		{
			Vector2 cache = GetPosition();

			cache.x += GetWidth() * _offset;
			cache.y += GetHeight() * _offset;

			return cache;
		}

		public virtual Vector3 GetLeftUpperCorner ()
		{
			Vector2 cache = GetPosition();

			cache.x -= GetWidth() * _offset;
			cache.y += GetHeight() * _offset;

			return cache;
		}

		public virtual Vector3 GetRightLowerCorner ()
		{
			Vector2 cache = GetPosition();

			cache.x += GetWidth() * _offset;
			cache.y -= GetHeight() * _offset;

			return cache;
		}

		public virtual Vector3 GetLeftLowerCorner ()
		{
			Vector2 cache = GetPosition();

			cache.x -= GetWidth() * _offset;
			cache.y -= GetHeight() * _offset;

			return cache;
		}
	}
}
