﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Persistable;
using System.Linq;

namespace PotionSoup.PrefabManagment
{
	public class SimplePrefabManager : Singleton<SimplePrefabManager> 
	{
		[System.Serializable]
		public class PrefabData
		{
			public string name;
			public GameObject prefab;
		}

		public List< PrefabData > PrefabDatas = new List<PrefabData> ();

		public static GameObject Get ( string item )
		{
			if (Instance.PrefabDatas.Count <= 0)
			{
				Debug.LogError ("Prefab data is empty, please assign some prefabs in the inspector.");
				return null;
			}

			IEnumerable<PrefabData> data = Instance.PrefabDatas.Where(x=> x.name == item);

			if (data.Count() <= 0)
			{
				Debug.LogError ( "couldn't find prefab by name: " + item);
				return null;
			}

			return data.First().prefab;
		}

	}
}
