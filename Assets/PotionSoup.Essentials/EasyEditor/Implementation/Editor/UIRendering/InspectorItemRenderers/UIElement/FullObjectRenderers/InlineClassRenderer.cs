//
// Copyright (c) 2016 Easy Editor 
// All Rights Reserved 
//  
//

using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace EasyEditor
{
    /// <summary>
    /// Renders a custom class or structure directly into the monobehaviour/scriptableobject inspector.
    /// </summary>
    public class InlineClassRenderer : FullObjecRenderer 
    {
        private object subtarget;
        private string foldoutTitle = "";
        private bool foldout = true;

        protected override void InitializeRenderersList()
        {
            base.InitializeRenderersList();

            subtarget = FieldInfoHelper.GetObjectFromPath(entityInfo.propertyPath, _serializedObject.targetObject);
            List<InspectorItemRenderer> fieldsRenderers = RendererFinder.GetListOfFields(subtarget, entityInfo.propertyPath);
            List<InspectorItemRenderer> methodsRenderers = RendererFinder.GetListOfMethods(subtarget);
            
            renderers = new List<InspectorItemRenderer>();
            renderers.AddRange(fieldsRenderers);
            renderers.AddRange(methodsRenderers);
            
            InspectorItemRendererOrderComparer comparer = new InspectorItemRendererOrderComparer(groups, renderers);
            renderers.Sort(comparer);
            
            foreach (InspectorItemRenderer renderer in renderers)
            {
                renderer.serializedObject = _serializedObject;
            }
        }

        protected override void RetrieveGroupList()
        {
            GroupsAttribute groupAttribute = AttributeHelper.GetAttribute<GroupsAttribute>(entityInfo.fieldInfo.FieldType);
            if (groupAttribute != null)
            {
                groups = new Groups(groupAttribute.groups);
            }
            else
            {
                groups = new Groups(new string[]{""});
            }
        }

        public override void Render(Action preRender = null)
        {
            if (string.IsNullOrEmpty(foldoutTitle))
            {
                foldoutTitle = ObjectNames.NicifyVariableName(entityInfo.fieldInfo.Name);
            }

            foldout = EditorGUILayout.Foldout(foldout, foldoutTitle);
            if (foldout)
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Space(15 * Settings.indentation);

                base.Render(preRender);

                EditorGUILayout.EndHorizontal();
            }
        }

        VisibilitySetter visibilitySetter;
        /// <summary>
        /// Sets the visibility of a renderer based on the attribute [Visibility]. 
        /// If VisibilityAttribute parameters id and value are not null, and the object with the id 'id' 
        /// has the value 'value', then the renderer holding the attribute is visible, otherwise it is not display in the inspector.
        /// If method is not empty, then if the class where the visibility attribute is used has this function and this function return true,
        /// the renderer will be rendered.
        /// </summary>
        protected override void SetVisibility()
        {
            if (visibilitySetter == null)
            {
                visibilitySetter = new VisibilitySetter();
            }

            foreach (InspectorItemRenderer renderer in renderers)
            {
                VisibilityAttribute visibilityAttribute = AttributeHelper.GetAttribute<VisibilityAttribute>(renderer.entityInfo);
                if(visibilityAttribute != null)
                {
                    visibilitySetter.Initialize(renderer, subtarget, subtarget, renderers.ToArray());
                    visibilitySetter.SetVisibility();
                }  
            }
        }

        //// <summary>
        /// Gets the message renderer for <c>InspectorItemRenderer</c> with the attribute MessageAttribute.
        /// </summary>
        /// <returns>The message renderer created to render MessageAttribute in the inspector.</returns>
        /// <param name="renderer">An item renderer.</param>
        protected override MessageRenderer[] GetMessageRenderers(InspectorItemRenderer renderer)
        {
            List<MessageRenderer> result = new List<MessageRenderer>();
            
            MessageAttribute[] messageAttributes = AttributeHelper.GetAttributes<MessageAttribute>(renderer.entityInfo);
            if(messageAttributes != null)
            {
                foreach(MessageAttribute messageAttribute in messageAttributes)
                {
                    result.Add(new MessageRenderer(messageAttribute, subtarget, subtarget, renderers.ToArray()));
                }
            }   
            
            return result.ToArray();
        }
    }
}