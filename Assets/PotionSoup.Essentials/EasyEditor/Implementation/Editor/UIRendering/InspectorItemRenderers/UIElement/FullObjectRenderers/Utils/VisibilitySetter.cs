﻿//
// Copyright (c) 2016 Easy Editor 
// All Rights Reserved 
//  
//

using UnityEngine;
using System.Collections;
using System.Reflection;

namespace EasyEditor
{
    public class VisibilitySetter
    {

        InspectorItemRenderer inspectorItemRenderer;

        string method = "";
        string id = "";
        object[] values;
        
        InspectorItemRenderer[] otherRenderers;
        object caller;
        object classFieldBelongTo;

        public void Initialize(InspectorItemRenderer inspectorItemRenderer, object caller, object classFieldBelongTo, InspectorItemRenderer[] otherRenderers = null)
        {
            VisibilityAttribute visibilityAttribute = AttributeHelper.GetAttribute<VisibilityAttribute>(inspectorItemRenderer.entityInfo);

            this.method = visibilityAttribute.method;
            this.id = visibilityAttribute.id;
            this.values = visibilityAttribute.values;
            this.caller = caller;
            this.classFieldBelongTo = classFieldBelongTo;
            this.inspectorItemRenderer = inspectorItemRenderer;
            this.otherRenderers = otherRenderers;
        }

        public void SetVisibility()
        {
            bool renderRenderer = true;
            
            if (!string.IsNullOrEmpty(method))
            {
                MethodInfo methodInfo = caller.GetType().GetMethod(method, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
                if (methodInfo != null)
                {
                    if (methodInfo.ReturnType == typeof(bool))
                    {
                        renderRenderer = (bool)methodInfo.Invoke(caller, null);
                    }
                    else
                    {
                        Debug.LogError("The method specified in the attribute Visibility have to return a bool.");
                    }
                }
                else
                {
                    Debug.LogError("The method specified in the attribute Visibility does not exist.");
                }
            }
            else if (!string.IsNullOrEmpty(id) && values != null && values.Length > 0)
            {
                InspectorItemRenderer conditionalRenderer = InspectorItemRenderer.LookForRenderer(id, otherRenderers);

                renderRenderer = false;

                if (conditionalRenderer != null && conditionalRenderer.entityInfo.isField)
                {
                    foreach ( var value in values  )
                    {
                        if (value.Equals(conditionalRenderer.entityInfo.fieldInfo.GetValue(classFieldBelongTo)))
                        {
                            renderRenderer = true; break;
                        }
                    }
                }
                else
                {
                    Debug.LogWarning("The identifier " + id + " was not found in the list of renderers, or this renderer " +
                        "was not initialized from a field. Ensure that the id parameter of the attribute Visibility refers to the id of a field " +
                        "(name of the field if you did not specify explicitly the id of the field in [Inspector(id = \"...\").");
                }
            }
            
            if (renderRenderer)
            {
                inspectorItemRenderer.hidden = false;
            }
            else
            {
                inspectorItemRenderer.hidden = true;
            }
        }
    }
}