﻿//
// Copyright (c) 2016 Easy Editor 
// All Rights Reserved 
//  
//

using UnityEngine;
using UnityEditor;
using UEObject = UnityEngine.Object;
using System.Collections;
using EasyEditor.ReorderableList;

namespace EasyEditor
{
    public class EESerializedPropertyAdaptor : SerializedPropertyAdaptor, IReorderableListDropTarget
    {
        private bool isReadOnly;

        public EESerializedPropertyAdaptor(SerializedProperty arrayProperty, float fixedItemHeight, bool isReadOnly) : base(arrayProperty, fixedItemHeight)
        {
            this.isReadOnly = isReadOnly;
        }

        private Rect DropTargetPosition 
        {
            get 
            {
                // Expand size of drop target slightly so that it is easier to drop.
                Rect dropPosition = ReorderableListGUI.CurrentListPosition;
                dropPosition.y -= 10;
                dropPosition.height += 15;
                return dropPosition;
            }
        }

        public bool CanDropInsert(int insertionIndex)
        {
            bool canDrop = true;

            canDrop &= !isReadOnly;
            canDrop &= IsObjectArray();

            if(DropTargetPosition.Contains(Event.current.mousePosition))
                canDrop &= true;

            return canDrop;
        }

        public void ProcessDropInsertion(int insertionIndex)
        {
            if (Event.current.type == EventType.DragPerform) 
            {
                for(int i = 0; i < DragAndDrop.objectReferences.Length; i++)
                {
                    _arrayProperty.InsertArrayElementAtIndex(insertionIndex + i);
                    _arrayProperty.GetArrayElementAtIndex(insertionIndex + i).objectReferenceValue = DragAndDrop.objectReferences[i];
                }
            }
        }

        private bool isObjectArrayWasChecked = false;
        private bool isObjectArrayCache;
        private bool IsObjectArray()
        {
            if(!isObjectArrayWasChecked)
            {
                isObjectArrayWasChecked = true;

                isObjectArrayCache = false;

                _arrayProperty.arraySize ++;
                if(_arrayProperty.GetArrayElementAtIndex(_arrayProperty.arraySize - 1).propertyType == SerializedPropertyType.ObjectReference)
                {
                    isObjectArrayCache = true;

                    // Unity doesn't remove element when it contains an object reference.
                    _arrayProperty.GetArrayElementAtIndex(_arrayProperty.arraySize - 1).objectReferenceValue = null;
                }
                _arrayProperty.DeleteArrayElementAtIndex(_arrayProperty.arraySize - 1);

                return isObjectArrayCache;
            }

            return isObjectArrayCache;
        }
    }
}