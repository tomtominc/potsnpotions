﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public static class SOGUI
{
	#region MenuTools

	static public int DrawSelectableHeader(string aSelectionID, params string[] aText)
	{
		GUILayout.Space(3f);
		GUILayout.BeginHorizontal();
		GUILayout.Space(3f);
		
		GUI.changed = false;

		int selectedHeader = EditorPrefs.GetInt("SelectableHeader_" + aSelectionID, 0);
		for(int i = 0 ; i < aText.Length ; i++)
		{
			string text = "<size=11>" + aText[i] + "</size>";
			if(i == selectedHeader)
			{
				text = "<b>" + text + "</b>";
			}
			if(!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
			{
				selectedHeader = i;
			}
		}
		EditorPrefs.SetInt("SelectableHeader_" + aSelectionID, selectedHeader);

		GUILayout.Space(2f);
		GUILayout.EndHorizontal();

		return selectedHeader;
	}

	static public bool DrawHeader (string aText, bool aDefaultValue = true, bool aForceDefault = false)
	{
		if(aForceDefault)
		{
			EditorPrefs.SetBool("DrawHeader_" + aText, aDefaultValue);
		}
		bool clicked = EditorPrefs.GetBool("DrawHeader_" + aText, aDefaultValue);
		GUILayout.Space(3f);
		GUILayout.BeginHorizontal();
		GUILayout.Space(3f);

		string text = "<size=11>" + aText + "</size>";
		if(clicked)
		{
			text = "<b>" + text + "</b>";
		}
		if(!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
		{
			clicked = !clicked;
			EditorPrefs.SetBool("DrawHeader_" + aText, clicked);
		}
		
		GUILayout.Space(2f);
		GUILayout.EndHorizontal();

		return clicked;
	}
	
	static public Rect DrawBox(string aLabel, float aHeight, Color aBackgroundColor, params GUILayoutOption[] options)
	{
		EditorGUILayout.BeginHorizontal();
		GUILayout.Space(3f);
		
		GUIStyle style = new GUIStyle("AS TextArea");
		style.richText = true;
		style.alignment = TextAnchor.MiddleCenter;
		
		SetBackgroundColor(aBackgroundColor);
		Rect boxArea = GUILayoutUtility.GetRect (0.0f, aHeight, options);
		GUI.Box (boxArea, aLabel, style);
		RestoreBackgroundColor();
		
		GUILayout.Space(2f);
		EditorGUILayout.EndHorizontal();
		
		return boxArea;
	}

	static public void BeginContent(Color aBackgroundColor, params GUILayoutOption[] options)
	{
		EditorGUILayout.BeginHorizontal();
		GUILayout.Space(3f);
		
		GUIStyle style = new GUIStyle("TextArea");
		style.richText = true;
		style.margin = new RectOffset(0,0,0,0);
		
		SetBackgroundColor(aBackgroundColor);
		EditorGUILayout.BeginVertical(style, options);
	}

	static public void EndContent()
	{
		EditorGUILayout.EndVertical();

		RestoreBackgroundColor();
		
		GUILayout.Space(2f);
		EditorGUILayout.EndHorizontal();
	}
	
	#endregion
	
	#region Color Tools
	
	public static void SetEditorPrefColor(string aKey, Color aColor)
	{
		EditorPrefs.SetFloat(aKey + "_r", aColor.r);
		EditorPrefs.SetFloat(aKey + "_g", aColor.g);
		EditorPrefs.SetFloat(aKey + "_b", aColor.b);
		EditorPrefs.SetFloat(aKey + "_a", aColor.a);
	}
	public static Color GetEditorPrefColor(string aKey)
	{
		Color color = Color.white;
		color.r = EditorPrefs.GetFloat(aKey + "_r", 1f);
		color.g = EditorPrefs.GetFloat(aKey + "_g", 1f);
		color.b = EditorPrefs.GetFloat(aKey + "_b", 1f);
		color.a = EditorPrefs.GetFloat(aKey + "_a", 1f);
		return color;
	}

	private static Stack<Color> s_BackgroundColors = new Stack<Color>();
	public static void SetBackgroundColor(Color aBackground)
	{
		s_BackgroundColors.Push(GUI.backgroundColor);
		GUI.backgroundColor = aBackground;
	}
	
	public static void RestoreBackgroundColor()
	{
		GUI.backgroundColor = s_BackgroundColors.Pop();
	}
	
	#endregion
}
