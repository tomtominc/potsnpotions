using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public class ScriptableObjectWindow : EditorWindow
{
	#region Editor Menu

	[MenuItem("Tools/ScriptableObject", false, 500)]
	public static void OpenWindow()
	{
		ScriptableObjectWindow window = EditorWindow.GetWindow<ScriptableObjectWindow>("+1 Click SO");
		window.minSize = new Vector2(330f, 117f);
		window.Show();
		window.Focus();
	}

	#endregion

	#region Window

	private const string s_HeaderDropName = "Drag&Drop area";
	private const string s_HeaderBrowserName = "Scripts Browser";

	private const string s_DropBoxName = "<color=#ffffffff>Drag your ScriptableObject script here</color>";
	private const string s_BrowseBoxName = "<color=#ffffffff>Select your script</color>";
	
	private const string s_DisabledTitle = "Nothing to create";
	private const string s_NoSelectionTitle = "Select a script";

	private const string s_HeaderAssetName = "Asset : ";
	private const string s_CreateButtonTitle = "+1";
	private const string s_AssetRenameLabel = "Name your asset =>";
	private const string s_DeleteButtonTitle = "X";
	private const string s_DefaultAssetPath = "Assets";
	private const string s_DefaultPathFormat = "{0}/{1}{2}.asset";

	private const float s_DefaultBoxHeigt = 60f;
	private const float s_DefaultAssetHeight = 64f;

	private Color m_BackgroundColor = new Color(0.5f, 0.5f, 0.5f);

	private int m_SelectedHeader = 0;

	private List<ScriptNames> createdObjects = new List<ScriptNames>();
	private Vector2 m_ScrollPosition = Vector2.zero;

	List<ScriptablesInfos> contents = new List<ScriptablesInfos>();
	string selectedSI = string.Empty;
	string selectedSO = string.Empty;
	private ScriptNames m_selectedObject = null;
	private Vector2 m_LeftScroller = Vector2.zero;

	private string m_BrowserFilter = string.Empty;
	private bool m_CloseAll = true;
	private bool m_StateClosed = false;

	void OnSelectionChange()
	{
		this.Repaint();
	}

	void OnFocus()
	{
		contents.Clear();
	}

	void OnGUI()
	{
		HeaderGUI();

		GUILayout.Space(3f);
		SOGUI.DrawBox(string.Empty, 3.5f, Color.black, GUILayout.ExpandWidth(true));
		GUILayout.Space(2f);
		
		BottomGUI();

		if(Event.current.type == EventType.MouseUp)
		{
			GUI.FocusControl(null);
			Event.current.Use();
		}
	}
	
	void HeaderGUI ()
	{
		m_SelectedHeader = SOGUI.DrawSelectableHeader("DropAreaHeader", s_HeaderDropName, s_HeaderBrowserName);
		switch(m_SelectedHeader)
		{
		case 0:
			DropAreaGUI();
			break;
		case 1:
			ScriptBrowserGUI();
			break;
		}
	}

	void FetchAssemblies(string aFilter)
	{
		foreach (var assembly in System.AppDomain.CurrentDomain.GetAssemblies())
		{
			if(assembly.FullName.Contains("UnityEditor")) continue;
			foreach (var script in assembly.GetTypes())
			{
				if(script.IsSubclassOf(typeof(UnityEngine.ScriptableObject)) &&
				   !script.IsSubclassOf(typeof(EditorWindow)) &&
				   !script.IsSubclassOf(typeof(Editor)) &&
				   script.Name.ToLower().Contains(aFilter.ToLower()))
				{
					ScriptableObject obj = ScriptableObject.CreateInstance(script);
					if(obj != null)
					{
						MonoScript monoScript = MonoScript.FromScriptableObject(obj);
						if(monoScript != null)
						{
							int index = contents.FindIndex(a => a.m_AssemblyName == assembly.GetName().Name);
							if(index == -1)
							{
								contents.Add(new ScriptablesInfos(){m_AssemblyName = assembly.GetName().Name});
								index = contents.Count - 1;
							}
							contents[index].types.Add(monoScript);
						}
					}
				}
			}
		}
		contents.Sort((a, b) => a.m_AssemblyName.CompareTo(b.m_AssemblyName));
		foreach (var item in contents)
		{
			item.types.Sort((a, b) => a.name.CompareTo(b.name));
		}
	}

	void ScriptBrowserGUI()
	{
		SOGUI.BeginContent(m_BackgroundColor, GUILayout.ExpandWidth(true));

		GUILayout.Space(2f);
		SOGUI.SetBackgroundColor(Color.white);
		EditorGUILayout.BeginHorizontal();
		GUI.changed = false;
		m_BrowserFilter = EditorGUILayout.TextField(m_BrowserFilter, new GUIStyle("ToolbarSeachTextField"), GUILayout.ExpandWidth(true));
		if(GUILayout.Button(string.Empty, "ToolbarSeachCancelButton"))
		{
			if(string.IsNullOrEmpty(m_BrowserFilter))
			{
				GUI.changed = false;
			}
			else
			{
				m_BrowserFilter = string.Empty;
			}
			GUI.FocusControl(null);
		}
		if(GUI.changed)
		{
			contents.Clear();
		}
		EditorGUILayout.EndHorizontal();
		GUILayout.Space(2f);
		SOGUI.RestoreBackgroundColor();
		
		if(contents.Count == 0)
		{
			FetchAssemblies(m_BrowserFilter);
			m_StateClosed = !string.IsNullOrEmpty(m_BrowserFilter);
		}

		SOGUI.DrawBox(string.Empty, 4f, Color.white, GUILayout.ExpandWidth(true));
		GUILayout.Space(2f);
		
		SOGUI.SetBackgroundColor(Color.white);

		GUIStyle miniButton = new GUIStyle("minibutton");
		miniButton.richText = true;
		GUIStyle labelField = new GUIStyle("label");
		labelField.richText = true;
		labelField.alignment = TextAnchor.MiddleRight;

		m_LeftScroller = EditorGUILayout.BeginScrollView(m_LeftScroller);
		for(int i = 0 ; i < contents.Count ; i++)
		{
			bool forced = selectedSI == contents[i].m_AssemblyName;
			if(SOGUI.DrawHeader(contents[i].m_AssemblyName, m_StateClosed || forced, m_CloseAll || forced))
			{
				SOGUI.BeginContent(Color.white, GUILayout.ExpandWidth(true));
				string letter = string.Empty;
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.BeginVertical();
				for(int j = 0 ; j < contents[i].types.Count ; j++)
				{
					if(letter != contents[i].types[j].name.ToUpper()[0].ToString())
					{
						EditorGUILayout.EndVertical();
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.BeginHorizontal();
						letter = contents[i].types[j].name.ToUpper()[0].ToString();
						GUILayout.Label("<b>" + letter + "</b>", labelField, GUILayout.Width(16f));
						EditorGUILayout.BeginVertical();
					}
					bool selected = contents[i].types[j].name == selectedSO && forced;
					GUIContent content = new GUIContent(contents[i].types[j].name);
					if(selected)
					{
						SOGUI.SetBackgroundColor(new Color(0.24f, 0.48f, 0.91f));
						content.text = "<color=ffffffff>" + content.text + "</color>";
					}
					else
					{
						SOGUI.SetBackgroundColor(new Color(0.8f, 0.8f, 0.8f));
					}
					if(GUILayout.Button(content, miniButton, GUILayout.ExpandWidth(true)))
					{
						if(selected)
						{
							selectedSI = selectedSO = string.Empty;
							m_selectedObject = null;
						}
						else
						{
							selectedSO = contents[i].types[j].name;
							selectedSI = contents[i].m_AssemblyName;
							m_selectedObject = new ScriptNames(contents[i].types[j], contents[i].types[j].name);
						}
						GUI.FocusControl(null);
						this.Repaint();
					}
					SOGUI.RestoreBackgroundColor();
				}
				EditorGUILayout.EndVertical();
				EditorGUILayout.EndHorizontal();
				SOGUI.EndContent();
			}
			else
			{
				SOGUI.BeginContent(Color.grey, GUILayout.ExpandWidth(true));
				SOGUI.EndContent();
			}
		}
		m_CloseAll = false;
		EditorGUILayout.EndScrollView();

		SOGUI.RestoreBackgroundColor();
		SOGUI.EndContent();
	}

	void DropAreaGUI()
	{
		Rect drop_area = SOGUI.DrawBox (s_DropBoxName, s_DefaultBoxHeigt, m_BackgroundColor, GUILayout.ExpandWidth(true));
		
		Event evt = Event.current;
		switch (evt.type)
		{
		case EventType.DragUpdated:
			if (!drop_area.Contains (evt.mousePosition))
				return;

			bool oneObjectValid = GetValidObjects(false);
			if(oneObjectValid)
			{
				DragAndDrop.visualMode = DragAndDropVisualMode.Link;
			}
			else
			{
				DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
			}
			Event.current.Use();
			break;
		case EventType.DragPerform:
			
			if (!drop_area.Contains (evt.mousePosition))
				return;

			DragAndDrop.AcceptDrag ();
			GetValidObjects(true);
			Event.current.Use();
			GUI.FocusControl(null);
			break;
		}
	}

	void BottomGUI()
	{
		switch(m_SelectedHeader)
		{
		case 0:
			if(createdObjects.Count == 0)
			{
				GUILayout.Space(3f);
				EditorGUI.BeginDisabledGroup(true);
				SOGUI.DrawBox(s_DisabledTitle, s_DefaultBoxHeigt, Color.white, GUILayout.ExpandWidth(true));
				EditorGUI.EndDisabledGroup();
			}
			else
			{
				m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);
				for(int i = createdObjects.Count - 1 ; i >= 0 ; i--)
				{
					if(DrawScriptableBox(createdObjects[i]))
					{
						createdObjects.RemoveAt(i);
					}
				}
				EditorGUILayout.EndScrollView();
			}
			break;
		case 1:
			if(m_selectedObject == null || string.IsNullOrEmpty(m_selectedObject.m_Name))
			{
				EditorGUI.BeginDisabledGroup(true);
				GUILayout.Space(3f);
				SOGUI.DrawBox(s_NoSelectionTitle, s_DefaultBoxHeigt, Color.white, GUILayout.ExpandWidth(true));
				EditorGUI.EndDisabledGroup();
			}
			else if(DrawScriptableBox(m_selectedObject))
			{
				m_selectedObject = null;
				selectedSO = selectedSI = string.Empty;
			}
			break;
		}
		GUILayout.Space(5f);
	}

	bool DrawScriptableBox(ScriptNames script)
	{
		if(script.m_Script == null) return true;

		bool deletePressed = false;
		SOGUI.DrawHeader(s_HeaderAssetName + script.m_Script.name, false, true);
		Rect scriptableBox = SOGUI.DrawBox(string.Empty, s_DefaultAssetHeight, Color.gray, GUILayout.ExpandWidth(true));

		float truncatedHeight = scriptableBox.height - 18f;

		SOGUI.SetBackgroundColor(Color.cyan);
		Rect buttonLeftRect = new Rect(scriptableBox.xMin + truncatedHeight / 6f, scriptableBox.yMin + truncatedHeight / 6f, truncatedHeight / 1.5f, truncatedHeight / 1.5f);
		if(GUI.Button(buttonLeftRect, s_CreateButtonTitle))
		{
			CreateScriptableObject(script.m_Script, script.m_Name);
		}
		SOGUI.RestoreBackgroundColor();

		Rect textRect = new Rect(scriptableBox.xMin + truncatedHeight, scriptableBox.yMin + truncatedHeight / 2f - 9f, scriptableBox.width - truncatedHeight * 2f, 18f);
		script.m_Name = EditorGUI.TextField(textRect, s_AssetRenameLabel, script.m_Name, GUI.skin.box);

		SOGUI.SetBackgroundColor(Color.red);
		Rect buttonRect = new Rect(scriptableBox.xMax - 3f * truncatedHeight / 4f, scriptableBox.yMin + truncatedHeight / 4f, truncatedHeight / 2f, truncatedHeight / 2f);
		if(GUI.Button(buttonRect, s_DeleteButtonTitle))
		{
			deletePressed = true;
		}
		SOGUI.RestoreBackgroundColor();

		GUIStyle box = new GUIStyle("box");
		box.wordWrap = false;
		Rect pathPosition = new Rect(scriptableBox.xMin + 2f, scriptableBox.yMax - 20f, scriptableBox.width - 4f, 18f);
		string tooltippath = GetCreationPath(script.m_Name);
		string path = "Path : " + tooltippath;
		Vector2 cursorPosition = box.GetCursorPixelPosition(pathPosition, new GUIContent(path), path.Length);
		Vector2 cursorPositionLeft = box.GetCursorPixelPosition(pathPosition, new GUIContent(path), 0);
		float xWidth = cursorPosition.x - cursorPositionLeft.x;
		if(pathPosition.xMax != -1 && xWidth > pathPosition.width)
		{
			path = path.Remove(10) + "..." + path.Remove(0, 16 + path.Length - Mathf.CeilToInt((path.Length) * pathPosition.width / xWidth));
		}
		GUI.Label(pathPosition, new GUIContent(path, tooltippath), box);

		return deletePressed;
	}

	#endregion

	#region Scriptable Objects Tools
	
	bool GetValidObjects(bool addToObjects)
	{
		bool oneValid = false;
		foreach (Object dragged_object in DragAndDrop.objectReferences)
		{
			if(dragged_object is MonoScript)
			{
				MonoScript script = dragged_object as MonoScript;
				System.Type type = script.GetClass();
				if(type.IsSubclassOf(typeof(ScriptableObject)))
				{
					ScriptableObject obj = ScriptableObject.CreateInstance(type);
					if((obj.hideFlags & HideFlags.DontSave) != HideFlags.DontSave)
					{
						oneValid = true;
						if(addToObjects)
						{
							if(createdObjects.Find((a) => a.m_Script == script) == null)
							{
								createdObjects.Add(new ScriptNames(script, type.ToString()));
							}
						}
						else
						{
							break;
						}
					}
				}
			}
		}
		return oneValid;
	}

	void CreateScriptableObject(MonoScript aScript, string aName)
	{
		ScriptableObject newObj = ScriptableObject.CreateInstance(aScript.GetClass());
		string finalPath = GetCreationPath(aName);
		AssetDatabase.CreateAsset(newObj, finalPath);
		EditorGUIUtility.PingObject(newObj);
	}

	static string GetCreationPath(string aName)
	{
		string path = s_DefaultAssetPath;
		if(Selection.assetGUIDs != null && Selection.assetGUIDs.Length > 0)
		{
			string assetPath = AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[0]);
			if(Directory.Exists(Application.dataPath + "/../" + assetPath))
			{
				path = assetPath;
			}
			else
			{
				path = Path.GetDirectoryName(assetPath);
			}
		}
		int number = 0;
		string finalPath = string.Format(s_DefaultPathFormat, path, aName, string.Empty);
		while(File.Exists(Application.dataPath + "/../" + finalPath))
		{
			finalPath = string.Format(s_DefaultPathFormat, path, aName, ++number);
		}
		return finalPath;
	}

	#endregion

	#region Serializable Class

	[System.Serializable]
	private class ScriptNames
	{
		public MonoScript m_Script = null;
		public string m_Name = string.Empty;

		public ScriptNames(MonoScript aScript, string aName)
		{
			m_Script = aScript;
			m_Name = aName;
		}
	}
	
	[System.Serializable]
	private class ScriptablesInfos
	{
		public string m_AssemblyName = string.Empty;
		public List<MonoScript> types = new List<MonoScript>();
	}

	#endregion

	#region Quick Tool

	[MenuItem("Assets/Create/ScriptableObject", false, 201)]
	public static void CreateScriptableObject()
	{
		if(Selection.activeObject != null && Selection.activeObject is MonoScript)
		{
			MonoScript script = Selection.activeObject as MonoScript;
			if(script.GetClass().IsSubclassOf(typeof(ScriptableObject)))
			{
				ScriptableObject obj = ScriptableObject.CreateInstance(script.GetClass());
				AssetDatabase.CreateAsset(obj, GetCreationPath(script.name));
			}
		}
	}
	
	[MenuItem("Assets/Create/ScriptableObject", true)]
	public static bool CanCreateScriptableObject()
	{
		if(Selection.activeObject != null && Selection.activeObject is MonoScript)
		{
			MonoScript script = Selection.activeObject as MonoScript;
			if(script.GetClass().IsSubclassOf(typeof(ScriptableObject)))
			{
				return true;
			}
		}
		return false;
	}

	#endregion
}
